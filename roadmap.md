[[_TOC_]]

# Introduction

The purpose of this document is to describe planned features of the forest observatory
trade website and related back end infrastructure.

See also:

- The inspiration section of the [visualizations/README.md](visualizations/README.md).

- Technical issues and bugs related to the implementation can be further discussed in 
  the [issues section of the gitlab 
  repository](https://gitlab.com/paulrougieux/env_impact_imports/-/issues).

## Product scope

The 6 products requested for the country fact sheets data on deforestation and forest 
degradation. The following commodities have been selected: **soya, cocoa, coffee, palm 
oil, wood, beef**.

Trade matrix information and agricultural Yield information for those products in a 
couple of CSV files  http://www.fao.org/faostat/en/#data/TM

In fact the FAOSTAT crop production (and Yield) data is already copied in the git repo 
https://gitlab.com/bioeconomy/env_impact_imports/-/tree/master/data/faostat


## Research questions

We want to highlight the land impact of changes in trade flows so that we can see if 
they are potentially related to deforestation and forest degradations. We frame this 
along the following research questions:

1. Which are the main suppliers of specific commodities potentially associated with 
   deforestation, starting with: soya, cocoa, coffee, palm oil, wood, beef

2. Which are the main export destinations from a particular tropical country, by 
   continent?

3. Which are the main commodities produced in each tropical country? Of these which are 
   the main ones exported to the EU?

4. **Land footprint**. What is the equivalent land area needed to produce the commodity 
   exported from a tropical country to the EU?  Expressed in hectares of land in 
   producing countries. The question is easier to answer for trade of primary products, 
   requires a life cycle analysis approach for secondary and finished products to 
   convert them to their equivalent amount of primary products.


## Site structure 

The Website will be organised along several interrelated narratives.

- Product narratives
- Country narratives

### Visualisation

List of visualisations for each research question.

1. Which are the main suppliers of soya, cocoa, coffee, palm oil, wood, beef?

    - Tree map visualization of EU imports by **weight** in one year.
    - Time series, stacked area chart to see the trend over time.
    - ... other plots ....

2. Which are the main export destinations by continent?

   - Map with arrow thickness representing trade volume in USD or tons. 

3. Which are the main commodities produced in each tropical country?

   - Tree map visualization of crop **production area** in one year.

4. **Virtual land**. What is the equivalent land area needed to produce an exported 
   commodity? 

   - Ideally this tree map should be transformed in terms of area as well (hard).


# Data

Modelling and visualization digest the existing data in a concise form to make it more
understandable. This paragraph describes the data structure so that we can now what is
possible in terms of modelling and visualization.

Bilateral trade data can be seen in the form of a multi dimensional matrix (or data
cube, or tensor). We have products in columns and reporter countries in rows, then
partner countries in the third dimension and time in the fourth dimension. The cells in
the multi dimensional matrix contain either trade values in euros, weights in kg, or
quantities in various unit specified in the unit column (such as m3, number of units,
linear meters, etc.).

The periodicity is monthly or yearly.


## TODO

Add FAOSTAT land use data, specifically the categories ‘Arable land and permanent crops’ 
and ‘Permanent meadows and pastures’.

- Time series of **harvest area** by crops only main commodities and
- Table with relative change compared to the last few years. Delta plot with harvest 
  area for the different plots.

Plot production 


## Naming conventions and code style

To harmonize column and variable names, please keep note of the configuration file
called
[env_impact_imports/config_data/column_names.csv](env_impact_imports/config_data/column_names.csv)
in the python part of this repository. Try to use variable names from the "jrc" column
in your data handling and plotting code to increase readability throughout the stack if
possible.

To ensure consistent style of the python code, the black auto formatter and the flake8
linter are used as a pre commit hook.

- repo: https://github.com/ambv/black
- repo: https://gitlab.com/pycqa/flake8



