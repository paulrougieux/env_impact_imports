const years = ["2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019"];

let options = "";
for (let i = years.length - 1; i >= 0; i--)
   options += "<option value='" + years[i] + "'>" + years[i] + "</option>";
document.getElementById("years").insertAdjacentHTML("afterbegin", options);

function changeYear() {
  max = null;
  const year = document.getElementById("years").value;
  redraw(year);
}

let response, max = null;

const format = d3.format(".1f"),
  color = d3.scaleOrdinal(["#e8054f", "#c412ff", "#2305e8", "#1984ff", "#ff4910"]);

const svgTreemapHeader = d3.select("#treemap-header"),
  widthTreemapHeader = +svgTreemapHeader.attr("width"),
  heightTreemapHeader = +svgTreemapHeader.attr("height");

const bar = svgTreemapHeader.append("g");

bar.append("rect")
  .attr("width", widthTreemapHeader)
  .attr("height", heightTreemapHeader)
  .attr("rx", 3)
  .attr("ry", 3)
  .style("fill", "#1984ff");

bar.append("text")
  .attr("x", "50%")
  .attr("y", "50%")
  .attr("dominant-baseline", "middle")
  .attr("text-anchor", "middle")
  .attr("font-size", "18px")
  .attr("font-weight", "600")
  .attr("fill",  "#ffffff");

const svgTreemap = d3.select("#treemap"),
  widthTreemap = +svgTreemap.attr("width"),
  heightTreemap = +svgTreemap.attr("height");

const treemap = d3.treemap()
  .tile(d3.treemapDice)
  .size([widthTreemap, heightTreemap])
  .paddingTop(16)
  .paddingInner(1);

Array.prototype.max = function() {
  return Math.max.apply(null, this);
};

function findArrayMax(parentId) {
  for (let i = 0; i < data.children.length; i++) {
    if (data.children[i].id == parentId) {
      const parentArray = [];
      for (let j = 0; j < data.children[i].children.length; j++)
        parentArray.push(data.children[i].children[j]["area"]);
      return parentArray.max();
    }
  }
}

function setOpacity(parentMax, value) {
  const opacity = d3.scaleLinear().domain([0, parentMax]).range([1, 0.2]);
  return opacity(value);
}

function wrapCellText(d, i, nodes) {
  let self = d3.select(this),
    text = self.text(),
    textLength = this.getComputedTextLength(),
    boundingBox = d3.select(this.parentNode.parentNode).select("rect").node().getBBox(),
    width = boundingBox.width,
    height = boundingBox.height;

  if (i === 0 || i === 1)
    self.attr("font-size", "16px");
  else
    self.attr("font-size", (height + width)/16 + "px");

  if (width < 10 || height < 20 || (height < (20 + parseFloat(getComputedStyle(nodes[i]).fontSize)) && i === 2))
    self.text("");
  else if (textLength > (width - 6)) {
    while (textLength > (width - 6)) {
      text = text.slice(0, -1);
      self.text(text + "...");
      textLength = self.node().getComputedTextLength();
    }
    if (text.lastIndexOf(" ") != -1) {
      text = text.substr(0, text.lastIndexOf(" "));
      self.text(text + "...");
    }
  }
}

function wrapBlockText(d) {
  let self = d3.select(this),
    text = self.text(),
    textLength = this.getComputedTextLength(),
    width = d.x1 - d.x0,
    height = d.y1 - d.y0;

  if (width < 30 || height < 30)
    self.text(".");
  else if (textLength > width) {
    while (textLength > width) {
      text = text.slice(0, -1);
      self.text(text + "...");
      textLength = self.node().getComputedTextLength();
    }
    if (text.lastIndexOf(" ") != -1) {
      text = text.substr(0, text.lastIndexOf(" "));
      self.text(text + "...");
    }
  }
}

function redraw (year) {
  data = response[year];

  bar.select("text").text("Total: " + data["total_area"].toLocaleString() + " ha");

  svgTreemap.selectAll("*").remove();

  const root = d3.hierarchy(data)
    .eachBefore(d => d.data.id = (d.parent ? d.parent.data.id + "." : "") + d.data.name)
    .sum(d => d.area);

  treemap(root);

  const cell = svgTreemap.selectAll("g")
    .data(root.leaves())
    .enter()
    .append("g")
    .attr("transform", d => "translate(" + d.x0 + ", " + d.y0 + ")");

  cell.append("rect")
    .attr("id", d => d.data.id)
    .attr("width", d => d.x1 - d.x0)
    .attr("height", d => d.y1 - d.y0)
    .attr("rx", 3)
    .attr("ry", 3)
    .style("fill", d => color(d.parent.data.id))
    .style("opacity", d => setOpacity(findArrayMax(d.parent.data.id), d.data.area));

  cell.append("text")
    .selectAll("tspan")
    .data(d => ["for", d.data.name].concat(format(d.data.area_percentage) + "%"))
    .enter()
    .append("tspan")
    .text(d => d)
    .attr("x", "5px")
    .attr("dy", "1em")
    .attr("font-weight", (d, i, nodes) => i === 0 ? "600" : "400")
    .attr("fill", "#ffffff")
    .each(wrapCellText);

  cell.append("title")
    .text(d => "consumed by: " + d.data.name + "\n" + "product: " + d.parent.data.name + "\n" + "area: " + d.data.area.toLocaleString() + " ha" + "\n" + "area percentage: " + format(d.data.area_percentage) + "%" + "\n" + "weight:" + d.data.weight.toLocaleString() + " hg" + "\n" + "yield: " + d.data.yield.toLocaleString() + " hg/ha");

  svgTreemap.selectAll("blocks")
    .data(root.descendants().filter(d => d.depth === 1))
    .enter()
    .append("text")
    .text(d => d.data.name)
    .attr("x", d => d.x0)
    .attr("y", d => d.y0 + 10)
    .attr("font-size", "18px")
    .attr("font-weight", "600")
    .attr("fill", d => color(d.data.id))
    .each(wrapBlockText);
}

response = dataBrazil;
redraw(2019);
