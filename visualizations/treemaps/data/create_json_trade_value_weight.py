# flake8: noqa
import pandas as pd
import json

df_importer_names = pd.read_csv("../../../data/metadata/reporter_names.csv")
df_exporter_names = pd.read_csv("../../../data/metadata/partner_names.csv")
df_product_names = pd.read_csv("../../../data/metadata/product_names.csv")


def createJson(importer_code, year, csv_file):
    df = pd.read_csv(csv_file, keep_default_na=False)

    df = df.query("reportercode == @importer_code & period == @year")

    data = {}
    data["name"] = df_importer_names.query("reportercode == @importer_code")[
        "reporter"
    ].tolist()[0]
    data["children"] = []

    for product_code in df.productcode.unique():
        data_part = {}
        data_part["name"] = df_product_names.query("productcode == @product_code")[
            "productdescription"
        ].tolist()[0]
        data_part["children"] = []
        data["children"].append(data_part)
        exporters = df.query(
            "reportercode == @importer_code & productcode == @product_code & period == @year"
        ).reset_index(drop=True)
        for i in range(len(exporters)):
            data_part_part = {}
            exporter_code = exporters["partnercode"][i]
            data_part_part["name"] = df_exporter_names.query(
                "partnercode == @exporter_code"
            )["partner"].tolist()[0]
            data_part_part["trade_value"] = exporters["tradevalue"][i]
            data_part_part["weight"] = exporters["weight"][i]
            data_part["children"].append(data_part_part)

    with open(
        "./output/trade_value_weight/meat-"
        + importer_code
        + "-products-"
        + year[:-2]
        + ".json",
        "w",
        encoding="utf-8",
    ) as f:
        json.dump(data, f, ensure_ascii=False, indent=4)

    data = {}
    data["name"] = df_importer_names.query("reportercode == @importer_code")[
        "reporter"
    ].tolist()[0]
    data["children"] = []

    for exporter_code in df.partnercode.unique():
        data_part = {}
        data_part["name"] = df_exporter_names.query("partnercode == @exporter_code")[
            "partner"
        ].tolist()[0]
        data_part["children"] = []
        data["children"].append(data_part)
        products = df.query(
            "reportercode == @importer_code & partnercode == @exporter_code & period == @year"
        ).reset_index(drop=True)
        for i in range(len(products)):
            data_part_part = {}
            product_code = products["productcode"][i]
            data_part_part["name"] = df_product_names.query(
                "productcode == @product_code"
            )["productdescription"].tolist()[0]
            data_part_part["trade_value"] = products["tradevalue"][i]
            data_part_part["weight"] = products["weight"][i]
            data_part["children"].append(data_part_part)

    with open(
        "./output/trade_value_weight/meat-"
        + importer_code
        + "-exporters-"
        + year[:-2]
        + ".json",
        "w",
        encoding="utf-8",
    ) as f:
        json.dump(data, f, ensure_ascii=False, indent=4)


# createJson('1', '200152', '../../../data/comext/extra_eu_imports_02_meat_2001_2009.csv')


def interpretValues(value):
    if value == "NA":
        return 0
    elif "e" in value:
        value_splitted = value.split("e")
        return int(value_splitted[0]) * (10 ** int(value_splitted[1]))
    else:
        return int(value)


df = pd.read_csv(
    "../../../data/comext/extra_eu_imports_02_meat_2001_2009.csv", keep_default_na=False
)

df = df.query('period == "200152"')

total_trade_value = 0
total_weight = 0

for i in range(len(df)):
    total_trade_value += interpretValues(df["tradevalue"][i])
    total_weight += interpretValues(df["weight"][i])

data_exporters = {}
data_exporters["total_trade_value"] = total_trade_value
data_exporters["total_weight"] = total_weight
data_exporters["name"] = "EU"
data_exporters["children"] = []

for exporter_code in df.partnercode.unique():
    data_exporters_part = {}
    data_exporters_part["name"] = df_exporter_names.query(
        "partnercode == @exporter_code"
    )["partner"].tolist()[0]
    data_exporters_part["children"] = []
    data_exporters["children"].append(data_exporters_part)

    exporters = df.query(
        'partnercode == @exporter_code & period == "200152"'
    ).reset_index(drop=True)

    for product_code in exporters.productcode.unique():
        exporters_products = exporters.query(
            "productcode == @product_code"
        ).reset_index(drop=True)

        trade_value = 0
        weight = 0
        for i in range(len(exporters_products)):
            trade_value += interpretValues(exporters_products["tradevalue"][i])
            weight += interpretValues(exporters_products["weight"][i])

        trade_value_percentage = (trade_value / total_trade_value) * 100
        weight_percentage = (weight / total_weight) * 100

        data_exporters_part_part = {}
        data_exporters_part_part["name"] = df_product_names.query(
            "productcode == @product_code"
        )["productdescription"].tolist()[0]
        data_exporters_part_part["trade_value"] = trade_value
        data_exporters_part_part["trade_value_percentage"] = trade_value_percentage
        data_exporters_part_part["weight"] = weight
        data_exporters_part_part["weight_percentage"] = weight_percentage
        data_exporters_part["children"].append(data_exporters_part_part)

with open(
    "./output/trade_value_weight/meat-EU-exporters-2001.json", "w", encoding="utf-8"
) as f:
    json.dump(data_exporters, f, ensure_ascii=False, indent=4)


data_products = {}
data_products["total_trade_value"] = total_trade_value
data_products["total_weight"] = total_weight
data_products["name"] = "EU"
data_products["children"] = []

for product_code in df.productcode.unique():
    data_products_part = {}
    data_products_part["name"] = df_product_names.query("productcode == @product_code")[
        "productdescription"
    ].tolist()[0]
    data_products_part["children"] = []
    data_products["children"].append(data_products_part)

    products = df.query(
        'productcode == @product_code & period == "200152"'
    ).reset_index(drop=True)

    for exporter_code in products.partnercode.unique():
        products_exporters = products.query(
            "partnercode == @exporter_code"
        ).reset_index(drop=True)

        trade_value = 0
        weight = 0
        for i in range(len(products_exporters)):
            trade_value += interpretValues(products_exporters["tradevalue"][i])
            weight += interpretValues(products_exporters["weight"][i])

        data_products_part_part = {}
        exporter = df_exporter_names.query("partnercode == @exporter_code")[
            "partner"
        ].tolist()[0]
        data_products_part_part["name"] = exporter
        data_products_part_part["trade_value"] = trade_value
        data_products_part_part["trade_value_percentage"] = (
            trade_value / total_trade_value
        ) * 100
        data_products_part_part["weight"] = weight
        data_products_part_part["weight_percentage"] = (weight / total_weight) * 100
        data_products_part["children"].append(data_products_part_part)

with open(
    "./output/trade_value_weight/meat-EU-products-2001.json", "w", encoding="utf-8"
) as f:
    json.dump(data_products, f, ensure_ascii=False, indent=4)
