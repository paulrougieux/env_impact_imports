# flake8: noqa
import pandas as pd
import glob
import json

# read the mapping between Comext and FAO product codes and eliminate the rows where the FAO product code is null
df_comext_fao_product_codes = pd.read_csv(
    "../../../data/metadata/hs_to_fao_product_codes.csv", keep_default_na=False
)
df_comext_fao_product_codes = df_comext_fao_product_codes.query(
    'productcode_fao != "NA"'
).reset_index(drop=True)

# print(df_comext_fao_product_codes)
# print(df_comext_fao_product_codes.dtypes)

# convert the 'productcode' column type to string
df_comext_fao_product_codes["productcode"] = df_comext_fao_product_codes[
    "productcode"
].astype(str)

# print(df_comext_fao_product_codes.dtypes)

# extract unique FAO product codes
fao_product_codes = []
for i in df_comext_fao_product_codes.index:
    fao_product_codes.append(df_comext_fao_product_codes["productcode_fao"][i])
fao_product_codes = list(set(fao_product_codes))

# print(fao_product_codes)

# extract Comext product codes
comext_product_codes = []
for i in df_comext_fao_product_codes.index:
    comext_product_codes.append(df_comext_fao_product_codes["productcode"][i])

# print(comext_product_codes)

# extract data for Brazil and the extracted product codes (from FAO)
df_fao = pd.read_csv(
    "../../../data/faostat/crop_production_all.csv",
    keep_default_na=False,
    encoding="latin-1",
)
df_fao_brazil = df_fao.query('Area == "Brazil"').reset_index(drop=True)
df_fao_brazil = df_fao_brazil[df_fao_brazil["Item Code"].isin(fao_product_codes)]

# with pd.option_context('display.max_rows', None):
# print (df_fao_brazil)

# print(df_fao_brazil.dtypes)

# convert the 'Value' column type to float
df_fao_brazil["Value"] = pd.to_numeric(df_fao_brazil["Value"])

# print(df_fao_brazil.dtypes)

# concatenate all the Comext files
comext_files = glob.glob("../../../data/comext/*.csv")

comext_data = []

for comext_file in comext_files:
    df_comext_single_file = pd.read_csv(comext_file)
    comext_data.append(df_comext_single_file)

df_comext = pd.concat(comext_data, ignore_index=True)

# print(df_comext)

# filter the rows where the exporter is Brazil and product code is in the list of the extracted product codes (in Comext data)
df_exporter_names = pd.read_csv("../../../data/metadata/partner_names.csv")
brazil_code = df_exporter_names.query('partner == "Brazil"').reset_index(drop=True)[
    "partnercode"
][0]

df_comext_brazil = df_comext.query("partnercode == @brazil_code")
df_comext_brazil = df_comext_brazil[
    df_comext_brazil["productcode"].isin(comext_product_codes)
]

# with pd.option_context('display.max_rows', None):
# print(df_comext_brazil)

# aggregate the weights (kg) for all the European countries and reorganize the data in a new dataframe
data = {"year": [], "product_code": [], "weight": []}
for year in df_comext_brazil.period.unique():
    for product_code in df_comext_brazil.productcode.unique():
        df_comext_brazil_year_product_code = df_comext_brazil.query(
            "period == @year & productcode == @product_code"
        ).reset_index(drop=True)
        # print(df_comext_brazil_year_product_code)
        weight = 0
        for i in df_comext_brazil_year_product_code.index:
            weight_single = df_comext_brazil_year_product_code["weight"][i]
            # print(weight_single)
            if pd.isnull(weight_single):
                # print('weight is null')
                weight_single = 0
            weight += weight_single
        # print('total weight: ' + str(weight))
        if weight != 0:
            data["year"].append(int(str(year)[:-2]))
            data["product_code"].append(product_code)
            data["weight"].append(weight * 10)  # convert kg to hg

# print(data)

df_comext_brazil_europe = pd.DataFrame(data)

# print(df_comext_brazil_europe)

# bring all together and produce the visualization files
data = {}
for year in df_comext_brazil_europe.year.unique():
    data_year = {}
    data_year["name"] = "Brazil"
    data_year["children"] = []

    total_area_harvested = 0
    for fao_product_code in fao_product_codes:
        fao_product = df_fao_brazil.query("`Item Code` == @fao_product_code").iloc[0][
            "Item"
        ]
        data_year_part = {}
        data_year_part["name"] = fao_product
        data_year_part["children"] = []
        # Comext product code(s) that correspond to the FAO product code
        comext_product_codes_subset = df_comext_fao_product_codes.query(
            "productcode_fao == @fao_product_code"
        )["productcode"].values.tolist()
        weight = 0
        for comext_product_code in comext_product_codes_subset:
            # print(year)
            # print(comext_product_code)
            # print(df_comext_brazil_europe.query('year == @year & product_code == @comext_product_code')['weight'])

            if not df_comext_brazil_europe.query(
                "year == @year & product_code == @comext_product_code"
            ).empty:
                weight += df_comext_brazil_europe.query(
                    "year == @year & product_code == @comext_product_code"
                ).iloc[0]["weight"]
        if weight > 0:
            # get the yield for the FAO product that corresponds to one or more Comext products (if more than one, their weights are summed in the for loop above) for a year
            corresponding_yield = df_fao_brazil.query(
                'Year == @year &`Item Code` == @fao_product_code & Element == "Yield"'
            ).iloc[0]["Value"]
            # calculate the harvested area for the portion of the FAO product production exported to Europe (area (ha) = weight (hg) / yield (hg/ha))
            # the units used in the FAO dataset:
            # area harvested: ha
            # yield: hg/ha
            # production: tonnes
            area_harvested_for_eu = weight / corresponding_yield
            area_harvested_for_all = df_fao_brazil.query(
                'Year == @year &`Item Code` == @fao_product_code & Element == "area_harvested"'
            ).iloc[0]["Value"]
            data_year_part_part = {}
            data_year_part_part["name"] = "EU"
            data_year_part_part["weight"] = weight  # unit: hg
            data_year_part_part["yield"] = corresponding_yield  # unit: hg/ha
            data_year_part_part["area"] = area_harvested_for_eu  # unit: ha
            data_year_part_part["area_percentage"] = (
                area_harvested_for_eu / area_harvested_for_all * 100
            )
            data_year_part["children"].append(data_year_part_part)

            data_year_part_part = {}
            data_year_part_part["name"] = "other"
            data_year_part_part["weight"] = (
                df_fao_brazil.query(
                    'Year == @year &`Item Code` == @fao_product_code & Element == "production"'
                ).iloc[0]["Value"]
                * 10000
                - weight
            )  # convert tonnes to hg
            data_year_part_part["yield"] = corresponding_yield  # unit: hg/ha
            data_year_part_part["area"] = (
                area_harvested_for_all - area_harvested_for_eu
            )  # unit: ha
            data_year_part_part["area_percentage"] = (
                data_year_part_part["area"] / area_harvested_for_all * 100
            )
            data_year_part["children"].append(data_year_part_part)

            data_year["children"].append(data_year_part)
            total_area_harvested += area_harvested_for_all

    data_year["total_area"] = total_area_harvested
    data[str(year)] = data_year

with open("./output/area_export/Brazil.json", "w", encoding="utf-8") as f:
    json.dump(data, f, ensure_ascii=False, indent=4)
