# built-in modules
from pathlib import Path

# third-party packages
import json
from biotrade.faostat import faostat

# output data directory
output_dir = Path.cwd() / "output/country"


def select_products_with_largest_area(years, reporter, threshold=90):
    """Selects products with the largest area based on the threshold."""

    table = faostat.db.tables["crop_production"]

    # build an SQL query using SQLAlchemy
    stmt = table.select().where(
        table.c.reporter == reporter,
        table.c.year.in_(years),
        table.c.element == "area_harvested",
        table.c.product_code < 1000,
    )

    # load the query result into a DataFrame
    df = faostat.db.read_sql_query(stmt)

    # sum the values for the same products in the given years
    df = (
        df.groupby("product").agg({"value": "sum"}).reset_index()
    )  # skipna is True for sum by default

    df["total_value"] = df.value.sum()
    df["value_percentage"] = df["value"] / df["total_value"] * 100

    # sort by percentage, compute the cumulative sum and shift it by one
    df.sort_values(by="value_percentage", ascending=False, inplace=True)
    df["cumsum"] = df.value_percentage.cumsum()  # skipna is True for cumsum by default
    df["cumsum_lag"] = df["cumsum"].transform("shift", fill_value=0)

    # label harvest areas above the threshold under a product called 'Others'
    # create a grouping variable called product_2, which will be 'Others' for products above the threshold
    df["product_2"] = df["product"].where(df["cumsum_lag"] < threshold, "Others")

    # group products that are in the 'Others' category and calculate their percentage
    index = ["product_2", "total_value"]
    df = df.groupby(index).agg({"value": "sum"}).reset_index()
    df["value_percentage"] = df["value"] / df["total_value"] * 100

    return df


def create_csv_json(years, reporter, threshold=90):
    """Create the JSON file for treemap visualization with D3.js.

    :param list years: the years to be selected
    :param str reporter: the reporter to be selected
    :param float threshold: selected products represent an area below this threshold, it can be greater than 0 and less than or equal to 100"""

    df = select_products_with_largest_area(years, reporter, threshold)

    file_name = reporter.lower().replace(" ", "_") + "_" + str(years[0])
    if len(years) > 1:
        file_name += "_" + str(years[-1])

    df.to_csv(output_dir / (file_name + ".csv"), index=False)

    data = {}
    data["name"] = reporter
    data["total_value"] = df["total_value"][0]
    data["children"] = []
    for i in range(len(df)):
        data_part = {}
        data_part["name"] = df["product_2"][i]
        data_part["value"] = df["value"][i]
        data_part["value_percentage"] = df["value_percentage"][i]
        data["children"].append(data_part)

    with open(output_dir / (file_name + ".json"), "w", encoding="utf-8") as f:
        json.dump(data, f, ensure_ascii=False, indent=4)


create_csv_json([2004, 2005, 2006, 2007, 2008], "Brazil")
create_csv_json([2009, 2010, 2011, 2012, 2013], "Brazil")
create_csv_json([2014, 2015, 2016, 2017, 2018], "Brazil")
create_csv_json([2019], "Brazil")

create_csv_json([2004, 2005, 2006, 2007, 2008], "Indonesia")
create_csv_json([2009, 2010, 2011, 2012, 2013], "Indonesia")
create_csv_json([2014, 2015, 2016, 2017, 2018], "Indonesia")
create_csv_json([2014, 2015, 2016, 2017, 2018], "Indonesia")
create_csv_json([2019], "Indonesia")

create_csv_json([2004, 2005, 2006, 2007, 2008], "Malaysia")
create_csv_json([2009, 2010, 2011, 2012, 2013], "Malaysia")
create_csv_json([2014, 2015, 2016, 2017, 2018], "Malaysia")
create_csv_json([2019], "Malaysia")

create_csv_json([2004, 2005, 2006, 2007, 2008], "Cameroon")
create_csv_json([2009, 2010, 2011, 2012, 2013], "Cameroon")
create_csv_json([2014, 2015, 2016, 2017, 2018], "Cameroon")
create_csv_json([2019], "Cameroon")

create_csv_json([2004, 2005, 2006, 2007, 2008], "Côte d'Ivoire")
create_csv_json([2009, 2010, 2011, 2012, 2013], "Côte d'Ivoire")
create_csv_json([2014, 2015, 2016, 2017, 2018], "Côte d'Ivoire")
create_csv_json([2019], "Côte d'Ivoire")
