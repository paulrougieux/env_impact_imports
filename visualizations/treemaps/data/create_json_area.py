import pandas as pd
import json

df = pd.read_csv(
    "../../../data/crop_production_all.csv", keep_default_na=False, encoding="latin-1"
)
df = df.query('Area == "Brazil" & Element == "Area harvested"').reset_index(drop=True)

data = {}

for year in df.Year.unique():
    df_year = df.query("Year == @year").reset_index(drop=True)

    total_value = 0
    for i in range(len(df_year)):
        if df_year["Item Code"][i] < 1000 and df_year["Value"][i] != "":
            total_value += float(df_year["Value"][i])

    data_year = {}
    data_year["name"] = "Brazil"
    data_year["total_value"] = total_value
    data_year["children"] = []
    for i in range(len(df_year)):
        if df_year["Item Code"][i] < 1000 and df_year["Value"][i] != "":
            data_year_part = {}
            data_year_part["name"] = df_year["Item"][i]
            value = float(df_year["Value"][i])
            data_year_part["value"] = value
            data_year_part["value_percentage"] = (value / total_value) * 100
            data_year["children"].append(data_year_part)

    data[str(year)] = data_year

with open("./output/area/Brazil.json", "w", encoding="utf-8") as f:
    json.dump(data, f, ensure_ascii=False, indent=4)
