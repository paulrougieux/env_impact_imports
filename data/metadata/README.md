
# Column names

The purpose of the [column_names.csv](column_names.csv) file is to harmonise variable 
names so that the software code becomes easier to understand across the different data 
sources. For example it is hard to understand that `ptTitle`  in UN Comtrade is related 
to `PartnerName` in the faostat forestry trade flows dataset and is called `area` in the 
faostat forestry production and crop production datasets. The software becomes easier to 
read if we agree to call all of these columns `partner`, to represent the partner 
country in a  bilateral trade flows as well as the country in an unilateral production 
dataset. Another example is `cmdCode` in UN Comtrade that uses the same codification as 
`PRODUCT_NC` in Eurostat Comext. FAOSTAT products (commodities) are called `item_code`.
The code becomes easier to read of we agree to call them all `product_code`.

The goal is to rename all input datasets with the harmonized columns names as soon as 
they are loaded into our system. The renaming procedure should use the correspondance 
table `column_names.csv` so that we can rely on this as a reference.

Data sources described in that table:

- `jrc` the reference name used in our code
- `comext` bilateral trade data from [Eurostat 
  Comext](https://ec.europa.eu/eurostat/estat-navtree-portlet-prod/BulkDownloadListing?sort=1&dir=%2Fcomext)
- `comtrade` bilateral trade data from [UN Comtrade](https://comtrade.un.org/data)
- `faostat_forestry_production` forestry production data
- `faostat_forestry_trade` bilateral [forestry trade flows from 
  FAOSTAT](http://www.fao.org/faostat/en/#data/FT)
- `faostat_crop_production` [crop production and yields from 
  FAOSTAT](http://www.fao.org/faostat/en/#data/QC)
- `faostat_crop_trade` bilateral crop trade flows (matrix) from FAOSTAT
- `faostat_rl` [land use from FAOSTAT](http://www.fao.org/faostat/en/#data/RL)
- `sql_type` SQL data type which can be used to create a database note for example how
  most product codes are numerical, but some contain characters. As a general rule
  product codes should be a character variable.

Note the faostat tables are in long format with element and value describing different
variables. For example the FAOSTAT crop production data QC has the following element
values: "Area harvested", "Yield", "Production". This table will be reshaped to wide
format in the code and these variables will end up as column names which we convert to
snake case variable names. The values taken by these elements are also entered inside
the column_names.csv harmonization table. The conversion should be done as early as
possible, if possible in the input data.

# Product codes


## Mapping table between FAOSTAT and UN Comtrade / HS system

The FAOSTAT mapping table available at 
https://www.fao.org/waicent/faoinfo/economic/faodef/annexe.htm#01
gives the correspondance between fao codes and HS codes for example:

| FAO code | description         | HS code    |
| -------- | ------------------- | ---------- |
| 0256     | PALMNUTS KERNELS    | 1207.10    |
| 0257     | OIL OF PALM         | 1511       |
| 0258     | OIL PALM KERNEL     | 1513.21,29 |
| 0259     | CAKE OF PALM KERNEL | 2306.60    |
| 0236     | SOYBEANS            | 1201       |
| 0237     | OIL OF SOYBEANS     | 1507       |
| 0238     | CAKE OF SOYBEANS    | 2304       |
| 0239     | SOYA SAUCE          | 2103.10    |
| 0240     | SOYA PASTE          | 2106ex     |
| 0241     | SOYA CURD           | 2106ex     |

## FAOSTAT product structure

FAOSTAT forestry products follow a nested structure described in 
https://www.fao.org/forestry/49962-0f43c0da7039a611aa884b3c6c642f4ac.pdf

For example Roundwood volumes are the sum of Industrial roundwood and wood fuel volumes. 
Industrial roundwood is itself divided in 3 sub products. Sawnwood volumes are the sum
of sawnwood coniferous and sawnwood non coniferous.

The `biotrade` package 
contains a function that compares the aggregate value to the sum of its constituents.



# Units

Faostat units

- Area harvested in hectares
- Yield in tons/hectares
- Production tonnes

Eurostat Comext units

- Trade values in euros
- Net mass is expressed in kilograms i.e. the weight of the goods without any packaging.
- The supplementary quantity is expressed in various units (m3, meters, ...) specified in the unit column.

See [Comext user guide](https://ec.europa.eu/eurostat/estat-navtree-portlet-prod/BulkDownloadListing?sort=1&file=comext%2FCOMEXT_METADATA%2FDOCS_AND_GUIDES%2FUser+guide++-+2016+edition.pdf)


# Bash commands to update the data

Download faostat data



Change column names and elements to our values based on the column_names.csv dataset

    perl -i'' -pE "s/$1/$2/g"
    # or sed 
    sed '1 s/^.*$/<?php/'

The 1 part only replaces the first line. 
https://stackoverflow.com/questions/9309940/sed-replace-first-line

