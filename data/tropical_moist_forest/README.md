# Description

Statistics of annual forest degradation and deforestation (Mha). 

Source: Tropical moist forest cover change from the [TMF 
explorer](https://forobs.jrc.ec.europa.eu/TMF/) associated publication "[Long-term 
(1990–2019) monitoring of forest cover changes in the humid 
tropics](https://doi.org/10.1126/sciadv.abe1603)".
