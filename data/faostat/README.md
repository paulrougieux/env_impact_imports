
Metadata from FAOSTAT

[[_TOC_]]


# Crops and livestock production

This is an extract of the metadata copied from the FAOSTAT website, for more information 
see http://www.fao.org/faostat/en/#data/QCL/metadata 

## 1) Contacts

Contact organisation Food and Agriculture Organization of the United Nations (FAO) 
Contact organisation unit Statistics Division (ESS) Contact name Mr. Salar Tayyib 
Contact person function Team Leader for Crops, Livestock and Food Statistics (CLFS) Team 
Contact mail address Viale delle Terme di Caracalla, 00153 Rome, Italy Contact email 
address faostat@fao.org Contact phone number +390657052548 Contact fax number


## 2) Action Date

Metadata last certified 2021-02-22 Metadata last posted 2021-02-22 Metadata last update 
2021-02-22


## 3) Main Characteristics data

Data description

> Crop statistics are recorded for 173 products, covering the following categories: 
> Crops Primary, Fibre Crops Primary, Cereals, Coarse Grain, Citrus Fruit, Fruit, Jute 
> Jute-like Fibres, Oilcakes Equivalent, Oil crops Primary, Pulses, Roots and Tubers, 
> Treenuts and Vegetables and Melons. Data are expressed in terms of area harvested, 
> production quantity and yield. The objective is to comprehensively cover production of 
> all primary crops for all countries and regions in the world.Cereals: Area and 
> production data on cereals relate to crops harvested for dry grain only. Cereal crops 
> harvested for hay or harvested green for food, feed or silage or used for grazing are 
> therefore excluded. Area data relate to harvested area. Some countries report sown or 
> cultivated area only

Classification system

> CPC 2.1 Expanded and FAO Commodity List (FCL), which are an internal FAO 
 classification systems. Primary classification Secondary classification Secondary 
 classification Sector coverage Most crop products under agricultural activity. 


Statistical concepts and definitions

> Areas refer to the area under cultivation. Area under cultivation means the area that 
> corresponds to the total sown area, but after the harvest it excludes ruined areas 
> (e.g. due to natural disasters). If the same land parcel is used twice in the same 
> year, the area of this parcel can be counted twice. For tree crops, some countries 
> provide data in terms of number of trees instead of in area. This number is then 
> converted to an area estimate using typical planting density conversions. Production 
> means the harvested production. Harvested production means production including 
> on-holding losses and wastage, quantities consumed directly on the farm and marketed 
> quantities, indicated in units of basic product weight. Harvest year means the 
> calendar year in which the harvest begins. Yield means the harvested production per ha 
> for the area under cultivation. Seed quantity comprises all amounts of the commodity 
> in question used during the reference period for reproductive purposes, such as seed 
> or seedlings. Whenever official data are not available, seed figures can be estimated 
> either as a percentage of production or by multiplying a seed rate (the average amount 
> of seed needed per hectare planted) with the planted area of the particular crop of 
> the subsequent year. Usually, the average seed rate in any given country does not vary 
> greatly from year to year.

Statistical unit Agriculture holdings cultivated for the production of crops.

Statistical population All areas cultivated with crops in a country.

Reference area All countries of the world and geographical aggregates according to the United Nations M-49 list.

Code - reference area

Code - Number of countries/areas covered

Time coverage

1961-2018 (up to 2017 for all elements computed from FBS framework, e.g. seed, derived/processed commodities)

Periodicity Annual

Start period


## 4) Units

Unit of Measure Production Quantity and Seed: tonnes
Unit of measure codes


# Crops and livestock trade flows / trade matrix

This is an extract of the metadata copied from the FAOSTAT website, for more information 
see  http://www.fao.org/faostat/en/#data/TM/metadata

## 1) Contacts

Contact organisation

Food and Agriculture Organization of the United Nations (FAO)

Contact organisation unit

Statistics Division (ESS)

Contact name

Mr. Salar Tayyib

Contact person function

Team Leader for Crops, Livestock and Food Statistics (CLFS) Team

Contact mail address

Viale delle Terme di Caracalla, 00153 Rome, Italy

Contact email address

faostat@fao.org

Contact phone number

390657052548

Contact fax number

## 2) Action Date

Metadata last certified 2020-07-29 Metadata last posted 2020-07-29 Metadata last update 
2020-07-29 


## 3) Main Characteristics data

Data description

> The food and agricultural trade dataset is collected, processed and disseminated by 
> FAO according to the standard International Merchandise Trade Statistics (IMTS) 
> Methodology. The data is mainly provided by UNSD, Eurostat, and other national 
> authorities as needed. This source data is checked for outliers, trade partner data is 
> used for non-reporting countries or missing cells, and data on food aid is added to 
> take into account total cross-border trade flows. The trade database includes the 
> following variables: export quantity, export value, import quantity, and import value. 
> The trade database includes all food and agricultural products imported/exported 
> annually by all the countries in the world.

Classification system

> HS 2017 converted into FAO Commodity List (also known as FCL, which is a 
> classification based on the item tree approach used for the compilation of SUA/FBS) is 
> used for mapping HS 2017 to CPC 2.1 Expanded.

Sector coverage

> The dataset contains all food and agricultural products imported and exported during 
> the reference year by country. In addition to the individual country data, other item 
> and country aggregates are disseminated. The processed trade data is essential for the 
> compilation of Supply/Utilization Accounts (SUA) and Food Balance Sheets (FBS).

Statistical concepts and definitions

> Quantity of food and agricultural exports: Export quantity is defined by the IMTS as 
> the physical quantity of domestic origin or manufactured products shipped out of the 
> country. It includes re-exports. According to the FAO methodology, the quantity of 
> food and agricultural exports included in the FAOSTAT database is expressed in terms 
> of weight (tonnes) for all commodities except for live animals which are expressed in 
> units (heads); poultry, rabbits, pigeons and other birds are expressed in thousand 
> units. As a general rule, trade quantity refers to net weight, excluding any sort of 
> container.Value of agricultural exports: Value of agricultural exports are expressed 
> in thousand US dollars in the FAOSTAT database. Export values are reported as FOB 
> (free on board???that is, the value of the goods plus the value of the services 
> performed to deliver the goods to the border of the exporting country).Quantity of 
> food and agricultural imports: Import quantity represents the physical quantity of the 
> products imported for domestic consumption or processing shipped into a country. It 
> includes re-imports. According to the FAO methodology, the quantity of food and 
> agricultural imports included in the FAOSTAT database is expressed in terms of weight 
> (tonnes) for all commodities except for live animals which are expressed in units 
> (heads); poultry, rabbits, pigeons and other birds are expressed in thousand units. As 
> a general rule, trade quantity refers to net weight, excluding any sort of container. 
> It includes also food aid quantities, where relevant.Value of agricultural imports: 
> Value of agricultural imports are expressed in thousand US dollars in the FAOSTAT 
> database. Import values are reported as CIF (Cost Insurance and Freight), that is, the 
> value of the goods, plus the value of the services performed to deliver goods to the 
> border of the exporting country, plus the value of the services performed to deliver 
> the good from the border of the exporting country to the border of the importing 
> country).

Statistical unit

> All crops and livestock products registered by the customs office in the country. In 
> case of non-customs trade data, the observation unit is the trade operator. For 
> example, within intra-EU trade statistics, this unit is any taxable person carrying 
> out intra-EU trade. For more information, see the IMTS compiler manual, edition 2012.

Statistical population

> All trade data on food and agricultural products, including livestock, are compiled by 
> all customs offices in the country. For intra-EU trade, the statistical population is 
> all trade operators recording trade transactions over a certain threshold. Total 
> merchandise import/export value is also included.

Reference area All countries of the world and geographical aggregates according to the FAO country and territory classification. 

Code - reference area

Code - Number of countries/areas covered

Time coverage

Annual data for the period 1961 - 2018


## 4) Units

Unit of Measure Export Quantity [t]Export Value [1000 USD]Import Quantity [t]Import 
Value [1000 USD] Unit of measure codes



# Forestry production and trade

Metadata copied from http://www.fao.org/faostat/en/#data/FO/metadata

## 1) Contacts

> Contact organisation Food and Agriculture Organization of the United Nations (FAO) 
> Contact organisation unit Forestry Division (NFO) Contact name Ashley Steel (Ms) 
> Contact person function Forestry Officer (Statistics) Contact mail address Viale delle 
> Terme di Caracalla, 00153 Rome, Italy Contact email address faostat@fao.org Contact 
> phone number +390657050745 Contact fax number +390657053945

## 2) Action Date

> Metadata last certified 2016-04-27 Metadata last posted 2021-06-22 Metadata last > 
> update 2021-06-22

## 3) Main Characteristics data

Data description 

> The database contains data on the production and trade in roundwood 
> and in primary wood and paper products for all countries and territories in the 
> world.The main types of primary forest products included in this database are 
> roundwood, sawnwood, wood-based panels, pulp, and paper and paperboard. These products 
> are detailed further and defined in the Joint Forest Sector Questionnaire (JFSQ) 
> (http://www.fao.org/forestry/statistics/80572/en/). The database contains details of 
> the following topics: - Roundwood removals (production) by coniferous and 
> non-coniferous wood, - production and trade in industrial Roundwood, sawnwood, 
> wood-based panels, wood charcoal, pulp, paper paperboard, and other products. More 
> detailed information on wood products, including definitions, can be found at 
> http://www.fao.org/forestry/statistics/80572/en/

Classification system

> FAO Forestry Commodity List (commonly used by Eurostat, ITTO and UNECE) is linked to 
> the Harmonized Commodity Description and Coding System (HS) codes (see 
> http://www.fao.org/forestry/statistics/80572/en/). Forestry Commodity List predates 
> the Central Product Classification of the United Nations (CPC) system and revisions 
> are underway. Primary classification Secondary classification Secondary classification 
> Sector coverage Forestry and logging, Manufacture of wood and wood products, 
> Manufacture of pulp, paper and paper products, collection of paper for recycling. 
> Statistical concepts and definitions Forestry statistics cover production and trade in 
> wood products by country. Production is reported in physical units of cubic metres 
> (m3) or tonnes (t), while trade is reported in both monetary (1000 US`$`) and physical 
> units (m3, t). Removals of roundwood comprise all quantities of wood felled and 
> removed from the forest and other wooded land or other felling sites. They are 
> measured in m3 under bark (without bark). Detailed information on wood products and 
> definitions can be found at: http://www.fao.org/forestry/statistics/80572/en/ 

Statistical unit

> The units for the data on removals are private owners, state owners and other public 
> owners of forests. For manufactured wood and paper products, the unit is the 
> enterprise. In each country, organisations/associations represent companies that 
> supply and use roundwood and/or trade in roundwood and wood products. Data are 
> generally collected from the users of roundwood (the wood industry companies or 
> companies that trade in roundwood). There are however different methods used for the 
> collection of basic data, e.g., in Sweden, the data on removals rely partly on the 
> National Forest Inventories. In Finland, data are collected from roundwood buyers, 
> i.e., wood industry companies.

Statistical population

> All producers of raw wood and of primary wood and paper products in a country. 
> Reference area All countries of the world and geographical aggregates according to the 
> FAO corporate standard on Country or Area Codes for Statistical Use Code - reference 
> area Code - Number of countries/areas covered Time coverage Annual data for the period 
> 1961 onwards. Periodicity Annual Start period 1961 End period Base period Not 
> applicable Base period code

## 4) Units

> Unit of Measure Production Quantity [m3], Production Quantity [t], Export Quantity 
> [m3], Export Quantity [t], Export Value [1000 USD], Import Quantity [m3], Import 
> Quantity [t], Import Value [1000 USD] Unit of measure codes FAO Corporate Standard on 
> Unit of Measure


# Forestry Trade flows

Metadata copied from 
http://www.fao.org/faostat/en/#data/FT/metadata

## 1) Contacts 

> Contact organisation Food and Agriculture Organization of the United Nations (FAO)
> Contact organisation unit Forestry Policy and Resources Division (FOA), Forestry
> Department Contact name Arvydas Lebedys (Mr) Contact person function Forestry Officer
> (Statistics) Contact mail address Viale delle Terme di Caracalla, 00153 Rome, Italy
> Contact email address faostat@fao.org Contact phone number +390657053641 Contact fax
> number +390657053945 

## 2) Action Date

> Metadata last certified 27/04/2016 Metadata last posted 27/04/2016 Metadata last update 27/04/2016

## 3) Main Characteristics data

Data description

> The database contains data on the bilateral trade flows in roundwood, primary wood and
> paper products for all countries and territories in the world. The main types of
> primary forest products included in are: roundwood, sawnwood, wood-based panels, pulp,
> and paper and paperboard. These products are detailed further. The definitions are
> available. More detailed information on wood products, including definitions, can be
> found at: http://www.fao.org/forestry/statistics/80572/en/ 

Classification system

> FAO Forestry Commodity List (commonly used with Eurostat, ITTO and UNECE) which is
> linked to the Harmonised System codes (see
> http://www.fao.org/forestry/statistics/80572/en/) Primary classification Secondary
> classification Secondary classification Sector coverage
> Forestry and logging, Manufacture of wood and wood products, Manufacture of pulp, paper and paper products, collection of paper for recycling.

Statistical concepts and definitions 

> Information is based on an analysis of > information provided by countries on the
> joint forest products questionnaire and from > data drawn from the Comtrade database
> of the United Nations Statistics Office. Where > volumes have not been reported in
> standard units, standard conversion factors have > been applied. Where reported
> volumes have been inconsistent with value, volumes have > been re-estimated on the
> basis of average unit values. In the absence of exporting > country reports, estimates
> of their exports have been constructed from importers' > reports ('mirror' trade
> statistics). 

> - Statistical unit Forestry and forest industry enterprises (local kind of activity units).
> 
> - Statistical population Assortments of raw wood and primary wood and paper products.
> 
> - Reference area All countries of the world and geographical aggregates according to the
>   United Nations M-49 list. Code - reference area Code - Number of countries/areas
>   covered 
> 
> - Time coverage Annual data for the period 1997 onwards. Periodicity Start period End
>   period Base period Not applicable Base period code 

## 4) Units

> Unit of Measure Export Quantity [m3]Export Quantity [t]Export Value [1000 USD]Import
> Quantity [m3]Import Quantity [t]Import Value [1000 USD] Unit of measure codes 
