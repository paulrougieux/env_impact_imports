
# Introduction

This project analyses the environmental impact of EU imports. The purpose is (1) to 
select the main bio based commodities imported by EU countries, (2) to find the main 
trade partners for each of these commodities and (3) to follow changes in imports 
through time. In addition, (4) agricultural yields data can be used to estimate the land 
footprint impacted by the imported quantities.


# Folder structure

The following folders are present in this repository:

* [data](data) contains the data divided in subfolder by sources. The metadata folder is 
  common to all sources.

* [notebooks](notebooks) contains R or jupyter notebooks used to analyse the data. 
  Notebooks can be exported to html pages or to pdf documents.

* [R](R) contains R scripts that format the data. [shiny](shiny) contains a shiny 
  (R-based) visualisation dashboard, currently broken.

* [site](site) contains the R Markdown source files for a draft website visible at 
  https://bioeconomy.gitlab.io/env_impact_imports/index.html
  The site menu is defined in `site/_site.yml`.

* [visualizations](visualizations) contains visualizations written in javascript.


# Input data

## Metadata and Naming convention

We harmonise variables names across sources using a naming convention defined in 
[data/metadata/column_names.csv](data/metadata/column_names.csv).

See the metadata document at [data/metadata/README.md](data/metadata/README.md). That
document details mapping table between product (commodities) codes in FAOSTAT and UN
Comtrade (HS) system. It also briefly mentions units. Additional metadata particular to
each source might be available in the particular data folders such as
[data/faostat/README.md](data/faostat/README.md).


## Data sources

- Trade in bio commodities from 

    - the Eurostat [Comext 
      database](https://ec.europa.eu/eurostat/estat-navtree-portlet-prod/BulkDownloadListing?sort=1&dir=comext).
    - the FAOSTAT crops and livestock trade flows (trade matrix) and the FAOSTAT 
      forestry trade flows
    - the UN Comtrade database

- Agricultural crop and livestock production and yields from the [FAOSTAT Crop 
  database](http://www.fao.org/faostat/en/#data/QC).
  Copied in this repository

- Tropical moist forest cover change from the [TMF 
  explorer](https://forobs.jrc.ec.europa.eu/TMF/) associated publication "[Long-term 
  (1990–2019) monitoring of forest cover changes in the humid 
  tropics](https://doi.org/10.1126/sciadv.abe1603)".
  The data was summarised at the country level in this repository, inside the 
  [data/tropical_moist_forest](data/tropical_moist_forest) folder.

The document [data/metadata/README.md](data/metadata/README.md) describes how we 
harmonized variable names between the different data sources.


## See also data input tools

Python packages

- [biotrade](https://gitlab.com/bioeconomy/biotrade)

R packages

- [eutradeflows](https://gitlab.com/paulrougieux/eutradeflows/)
- [tradeflows](https://github.com/paulrougieux/tradeflows)
- [FAOSTAT](https://gitlab.com/paulrougieux/faostatpackage/)


## Technical details on data formats

Most data is prepared in the form of csv files, saved under the data forlder within this 
git repository. Some R data frames  have been saved under the rds file format 
(Serialization Interface for R Objects). [Markdown 
notebooks](https://rmarkdown.rstudio.com/lesson-10.html) and Jupyter notebooks are used 
for the analysis. Each notebook can load data from csv files. See the notebook 
[bio_imports_update_cache.Rmd](bio_imports_update_cache.Rmd) for instructions on how to 
update the cached data.


### Eurostat Comext

#### Cached data inside this repository

Sorry the data flow has 2 intermediate steps because I originally saved the database 
content to an RDS file (R data serialization) then to csv files. The data and metadata 
flow is kind of like this:

    Database --> RDS file --> CSV files --> visualizations

The 2 steps in the middle are obviously unnecessary. They are here because the RDS file 
enables loading data on a laptop where the database server is not installed or not 
reachable. The CSV files enable loading data from other programming languages such as 
python or javascript. In addition each steps narrows down the scope of the data for the 
analysis. The database has a size greater than 2 Gb (contains all import and export 
tradeflows including intra and extra EU trade), the RDS file is around 180 Mb (contains 
only EU imports from extra Eu countries) and the csv files only contain the trade flows 
for 10 selected commodities.


#### Analysis performed in notebooks

CN commodities and product codes have 8 digits. The 2 first digit represent a high level 
aggregate and each additional digit refines the particular product segment considered. 
The 8 digit level being the most detailed. We have chosen to provide an overview at the 
2 digit level and then to choose the most important products at the 8 digit level. The 
notebook 
[overview_CN2_all_products.pdf](notebooks/CN2_overview/overview_CN2_all_products.pdf) 
(source code 
[overview_CN2_all_products.Rmd](notebooks/CN2_overview/overview_CN2_all_products.Rmd)) 
provides an overview of import volumes and values at the 2 digit level. The notebook 
`main_CN8.Rmd` selects the main commodities under each CN2 chapter. And the notebook 
[CN8_by_partners.pdf](notebooks/CN8_main_products/CN8_by_partners.pdf) (source code 
[CN8_by_partners.Rmd](notebooks/CN8_main_products/CN8_by_partners.Rmd)) selects the main 
partner countries for the most important commodities at the 8 digit level.


The analysis is performed in the following R markdown notebooks, converted to pdf files:

    notebooks/CN2_overview/overview_CN2_all_products.Rmd
    notebooks/CN2_overview/overview_CN2_selected.Rmd
    notebooks/CN8_main_products/CN8_by_partners.Rmd contains the most interesting 
    details on product groupings.
    notebooks/CN8_main_products/main_CN8.Rmd
    notebooks/CN8_with_crop_yields/CN8_by_partners_land_impact.Rmd
    notebooks/CN8_with_crop_yields/load_crop_yields_data.Rmd
    notebooks/mercosur/CN8_by_partners.Rmd
    notebooks/update_data/update_cache.Rmd

Not all pdf reports might be available or up to date. Contact the authors at the emails 
visible in the commit messages if you want to make the latest information available.


# Visualisation

Gitlab CI is used to build a draft website visible at: 
https://bioeconomy.gitlab.io/env_impact_imports/index.html


# Code formatting convention

The python code is formatted with the black auto formatter.

Install `pre-commit`

    pip install pre-commit

Update [hook repositories to the latest
version](https://pre-commit.com/#using-the-latest-version-for-a-repository)

    pre-commit autoupdate

Install git hooks in your .git/ directory.

    pre-commit install

