
# TODO

Address questions 1 and 2 in the short term and address question 3 in the long term.


## Back end

See the biotrade package.

- Update FAOSTAT cop trade matrix data

- Figure out API authentication issue from python


## Front end


### Short-term goals

Automation

- Add the python code that generates json files inside the docker container build
  commands `.gitlab-ci.yml` so that we don't need to save the json files inside the git
  repository.

- Implement data download and column renaming in bash. Aiming for a minimalist download 
  and pre-formating script that can be run as a cron job on any linux machine. 

Data preparation

- Add FAOSTAT yield values so that we can convert the treemap to equivalent land areas.

- **Conversion coefficient from commodity weight to area**. 
    - Use the Yield for raw crops such as cereals, maybe coffee. 
        - Check if it is correct how to relate soybean with CN code 12019000.
    - Requires more information as soon as the exported
      products are transformed, for example vegetable oils, cacao butter, sugar.  Ethanol production requires a
      computation of sugar cane area for ethanol and sugar can area for sugar.

Tree maps


1. Tree map of trade value from a particular country to Europe [Tree map of trade value 
  and weight from Brazil to 
  Europe](https://paulrougieux.gitlab.io/env_impact_imports/visualizations/treemaps/web/trade-value-weight/index.html)
  - add navigation/zooming or connection to country pages? Simple implementation using 
    radio buttons for the 10 CN2 products on the list. 
  - add total percentage for countries (where?)
  - The full 8 digit level details are too much to show. When by products and weight 
    radio buttons are selected, we want this map at a higher aggregated level. With only 
    the 2 digit level products and maybe the 4 digit levels as small subcolors in 
    addition to the countries. 

2. Tree map visualization of crop **production area** in one year.

    - Tree Map of **crop** area harvested in Brazil. Highlighting the proportion of
      national production that goes to EU. This requires a mapping table between FAOSTAT
      crop production and Eurostat Commodities. Using the Conversion coefficient from
      commodity weight to area. Use the Yield as a conversion coefficient for raw crops
      such as cereals, maybe coffee. 


Time series visualizations as line charts or stacked area charts.

- Visualize the time series of the sum of all converted land areas for all products in
  one country in parallel to the deforested area from Frédéric's team.

- Add FAOSTAT crop and meat production data to highlight which part of the production is 
  exported from the country.

- On a time series, you can click on a point a get a corresponding tree map in that
  year

- Stacked area charts:
  - create a stacked area chart of yearly harvested area change for all the products in Brazil for all the years and superimpose this with deforestation
  - create a stacked area chart of yearly production change for all the products in Brazil for all the years and create a line chart for deforestation
  - (alternatives: use percentages for changes with respect to the previous year and use line charts instead of a stacked area chart in the charts above)


Other visualizations 

- Research web technologies for creating flow maps (again)


### Long-term goals

- visualization:
    - add summary statistics
    - treemaps:
        - add a time slider
        - enable multiple countries selection

# Done

- treemaps (trade value and weight from Brazil to Europe):
    - use percentages in the rectangles, put the values in the tooltip (30/03/2021)
    - make the treemaps EU-level (30/03/2021)
    - make treemaps by products and countries (30/03/2021)
    - replace codes with values, crop the values after a certain number of characters and comma or space and add '...', put the complete value in the tooltip (30/03/2021)
- treemaps (harvested areas in Brazil):
    - create a treemap for harvested areas and a single year, provide a drop-down list for changing the year (15/04/2021)
- input data
    - Add FAOSTAT yield values (2021-04-12)


