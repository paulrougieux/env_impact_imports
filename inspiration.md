# Inspiration

The purpose of this document is to collect a list of online platforms visualizing trade data or information related to the environmental footprint.


## Mixture of several visualizations

* UN Comtrade [Exploring Trade Data](https://comtrade.un.org/labs/) shows several visualisation platforms worth exploring (some are linked below)
* [Harvard Growth Lab](https://growthlab.app/)
* [Trase](https://trase.earth/) "Brings unprecedented transparency to commodity supply chains revealing new pathways towards achieving a deforestation-free economy." (more at [Vizzuality](https://www.vizzuality.com/project/))
* [INForest](https://forest-data.unece.org/) Data and knowledge platform for forests in the UNECE region
* [The State of the World's Forests 2020 by FAO](http://www.fao.org/state-of-forests/en/) "Why does forest biological diversity matter? Find out with our State of the World’s Forests 2020 report."    
* [ComexVis](http://comexstat.mdic.gov.br/en/comex-vis)
* [Atlas of Sustainable Development Goals](https://datatopics.worldbank.org/sdgatlas/)
- [Flourish visualization platform](https://flourish.studio/examples/) provides video 
  like animations of bar charts and scatter plots through time.

## Maps

* [The globe of economic complexity](http://globe.cid.harvard.edu/?mode=productsphere&id=JPk)
* https://resourcetrade.earth/ & https://circulareconomy.earth/trade
* https://globalization.stern.nyu.edu/maps?country=USA&indicator=mx&year=2019&color=default

Maps with raster data

* https://encore.naturalcapital.finance/en/map
* https://lcviewer.vito.be/2015
* https://dopa-explorer.jrc.ec.europa.eu/
* https://econservation.jrc.ec.europa.eu/


## Sankey Diagrams

- Simple, readable Sankey Diagram, with origin and destination only. A good illustration of the breakdown between national consumption and exports to different world regions. [outsourcing deforestation the local vs the international footprint](https://earth.org/data_visualization/outsourcing-deforestation-the-local-vs-the-international-footprint/)


## Time series

- The Atlas of Economic Complexity by Harvard (note the URL parameters which enable deep linking to a certain visualization type)
    - [Brazil agriculture exports over time to the different world regions](https://atlas.cid.harvard.edu/explore/stack?country=32&year=2012&startYear=1995&productClass=HS&product=1&target=Product&partner=undefined)


## Treemaps

Treemaps are a certain type of visualization that use tiles of various size to represent the proportion of different components within a whole.

* [What is a trillion dollars?](https://informationisbeautiful.net/visualizations/trillions-what-is-a-trillion-dollars/) (more at informationisbeautiful.net)
* [The Observatory of Economic Complexity](https://oec.world/)
* [The Atlas of Economic Complexity by Harvard](https://atlas.cid.harvard.edu/explore)
    * [Brazil](https://atlas.cid.harvard.edu/explore/?year=2012&country=32&redirected=true)


## UI elements

User interface elements that facilitate country or product selection.

* Selectize enables multiple country selection with autocomplete: https://shiny.rstudio.com/gallery/selectize-examples.html
