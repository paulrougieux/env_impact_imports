---
title: "Update the cached data for the project on the environmental impact of EU imports."
author: "Paul Rougieux"
date: "18 March 2018"
output:
  pdf_document: 
    number_sections: yes
    toc: yes
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
knitr::opts_knit$set(root.dir="../..") # file paths are relative to the root of the project directory
library(eutradeflows)
library(dplyr)
library(tidyr)
library(ggplot2)
con <- RMariaDB::dbConnect(RMariaDB::MariaDB(), dbname = "tradeflows")
```


# Introduction

The purpose of this document is to update the cache in the form of rds files so the data
can be analysed on machines which do not have a database installation. 


See if the data can be loaded in memory. 

Pre processing of the input data:

* The imput data is loaded from comext is loaded to the database 
  with the trade harvester package (now merged into the eutradeflows package).
    see docs/harvest under the tradeharvester package. 
    section 2020 update, subsection on the laptop.
* It is then prepared and aggregated in a R script called R/env_impact.R


Object size 
* with 7 products 
    * object size before the filter and select statement 1.4 GB.
    * Object size after the filter and select statement 107.2 Mb.
* with 63 products 738.4 Mb and as rds file uses 87Mb.

# Download from Comext and transfer to DB

See eutradeflows/notebooks/harvest.Rmd the section on 2020 update on the
laptop/download yearly files. It involves the use of the following functions from
the `eutradeflows` package: `scraplistoffilesincomext`, `transfer7zfolder2db`,
`harvestcomextmetadata` and `cleanallcomextcodes`
I didn't copy it here to avoid duplication and because this data update is
generic and can serve the purpose of other projects.

# Transfer from the DB to the cache

For the purpose of this project, I save data from the database into a cache
file which makes it possible to use it without having to install a MySQL or
MariaDB database on the machine.

## Update the metadata cache 

Note metadata is loaded first here, since EU country codes are used later in the data query. 

```{r eval=FALSE}
# Load partner country codes and names
partner_names <- tbl(con, 'vld_comext_partner') %>% collect()
# Fix character encoding issue in Laos name (see Latex error in bio_imports_main_partner_countries.Rmd)
partner_names$partner[grepl("Lao", partner_names$partner)] <- "Lao"
# Load reporter country codes and names
reporter_names <- tbl(con, 'vld_comext_reporter') %>% collect()
# Load product codes and names
product_names <- tbl(con, 'vld_comext_product') %>% collect()
# Fix character encoding issue in some product names
product_names$productdescription <- gsub("�"," ", product_names$productdescription)
# The issue is not only with this "├" special character but also with those that follow 
# Truncate those to 40 characters
product_names$productdescription <- ifelse(grepl("├",product_names$productdescription),
       substr(product_names$productdescription,1,40),
       product_names$productdescription)

# Save the metadata in rds cache files
saveRDS(partner_names, "data/metadata/partner_names.rds")
saveRDS(reporter_names, "data/metadata/reporter_names.rds")
saveRDS(product_names, "data/metadata/product_names.rds")


# List of EU country codes used to select extra-eu trade only below
eu_codes <- reporter_names$reportercode
```

### Update the CSV files of product names

Save product descriptions in csv files.
TODO: save the produc_names to one csv file only.
This is currently performed in 

    R/cache_metadata.R|9| write.csv(product_names, "data/product_names.csv", row.names = FALSE)

```{r eval=FALSE}
# Product names at the 2 digit level only
product_names_HS2 <- product_names %>%
    filter(nchar(productcode) == 2)
product_names_HS2 %>%
    write.csv("data/product_names_CN2.csv", row.names=FALSE)

# Product names at the 4 digit level only
product_names %>%
    filter(nchar(productcode) == 4) %>%
    write.csv("data/product_names_CN4.csv", row.names=FALSE)

# Product names at the 6 digit level only
product_names %>%
    filter(nchar(productcode) == 6) %>%
    write.csv("data/product_names_CN6.csv", row.names=FALSE)
```


Product names at the 8 digit level only

```{r}
# Read product groupings to create an env_group column
env_group_mapping <-
    read.csv("data/productdescription_to_env_group.csv", stringsAsFactors = FALSE)

#' Associate an environmental group to a particular product description
#' @param description character product description
#' @param mapping data frame mapping a character string to a
#' particular group
#' @return a vector of env_group names the same length as the description input
#' vector
#' @example
#' pn <- head(product_names)
#' associate_env_group(pn$productdescription, env_group_mapping)
associate_env_group <- function(description, mapping){
    # Initialize empty output vector of group names
    env_group <- substr(description, 0, 0)
    for (s in mapping$search_for){
        group_name <- mapping$env_group[mapping$search_for == s]
        group_found <- ifelse(grepl(s, description, ignore.case=TRUE), group_name, "")
        # Append the group found to the env_group variable
        env_group = paste(env_group, group_found, sep=",")
    }
    # Replace repeated commas by only one comma
    env_group <- gsub('(,)+', ',', env_group)
    # Remove the first and last commas
    env_group <- gsub('^,|,$', '', env_group)
    return(env_group)
}

pn8 <- product_names %>%
    filter(nchar(productcode) == 8) %>%
    # Grouping for the purpose of the project environmental impact of EU import
    mutate(env_group = associate_env_group(productdescription, env_group_mapping)) %>%
    # First version of the grouping, easier to understand
    # mutate(env_group = ifelse(grepl("beef", productdescription, ignore.case=TRUE),
    #                           "beef", ""),
    #        env_group = ifelse(grepl("bovine", productdescription, ignore.case=TRUE),
    #                           "beef", "")) %>%
    select(productcode, env_group, productdescription)
pn8 %>%
    write.csv("data/product_names_CN8.csv", row.names=FALSE)
```


## Update the bilateral import data cache

```{r eval=FALSE}
yearly_tbl <- tbl(con, 'raw_comext_yearly_env_impact') %>% 
    # Only imports
    filter(flowcode == 1L &
               # Only Extra-EU partners
               !partnercode %in% eu_codes) %>% 
    select(c("reportercode", "reporteriso", "partnercode", "partneriso", 
             "productcode", "flowcode", "statregime", "unitcode", 
             "period", "tradevalue", "weight", "quantity"))
# Display the generated SQL query
print(show_query(yearly_tbl))
# Load the yearly table from the database to the memory
system.time(
    yearly <- yearly_tbl %>% collect()
)

# With 7 products:
# user  system elapsed 
# 1.706   0.310  76.138 
# With 63 products:
#   user  system elapsed 
# 25.017   2.662 697.931 

# Place taken in memory
print(object.size(yearly), units = "auto")
rm(yearly_tbl)

# Backup the output of the query
saveRDS(yearly, "data/comext/extra_eu_bio_imports.rds")
```

## Split file in 2010 and 2015

Since we store the file inside the git repository. We would like to split it in
2010 and 2015 so that it's only update when necessary.


### Save only products of interest

Save products of interest as csv files separated in 3 periods depending on the
update frequency. This will create smaller rds files that should be faster to
load and won't clog the git repository.

```{r eval=FALSE}
source("R/cache_selected_products_and_years.R")
separate_rds_into_selected_products_and_years()
```

Read data for the products of interest.
```{r eval=FALSE}
source("R/cache_selected_products_and_years.R")
bli <- read_selected_products_and_years()
```


# Read from the cache
```{r}
system.time(
    yearly <- readRDS("data/comext/extra_eu_bio_imports.rds")
)
# 15 seconds for 63 countries
#   user  system elapsed 
# 14.999   0.382  15.389 

yearly <- yearly %>% 
    mutate(year = period %/% 100,
           productcode2d = substr(productcode, 1,2)) 
```


# Disconnect from the DB
```{r}
RMariaDB::dbDisconnect(con)
```

