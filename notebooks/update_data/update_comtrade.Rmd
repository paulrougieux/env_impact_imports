---
title: "Update the cached UN Comtrade data for the project on the environmental impact of EU imports."
author: "Paul Rougieux"
date: "18 March 2018"
output:
  pdf_document: 
    number_sections: yes
    toc: yes
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
knitr::opts_knit$set(root.dir="../..") # file paths are relative to the root of the project directory
library(eutradeflows)
library(dplyr)
library(tidyr)
library(ggplot2)
library(tradeflows)
```

# Load Comtrade data with the tradeflows package

See tradeflows/vignettes/loadcomtrade.Rmd for an explanation of the tradeflows
package.

## Sawn wood data


```{r}
# Load trade data from Comtrade
# "other sawnwood" trade France in 2018
swd99fr <- loadcomtradebycode(440799, 251, 2018)
```


## Meat data

API call to load meat code 020110 trade data for Brazil in 2018

    /api/get?max=500&type=C&freq=A&px=HS&ps=2018&r=76&p=all&rg=all&cc=020110%2C02

Arguments:

- max=500 maximum number of rows
- type=C
- freq=A
- px=HS
- ps=2018 Periods
- r=76 Reporter code for Brazil
- p=all Ask to return all partner codes
- rg=all All trade flows
- cc=020110 Product code

API call for 5 years of data

    /api/get?max=500&type=C&freq=A&px=HS&ps=2016%2C2017%2C2018%2C2019%2C2020&r=76&p=all&rg=all&cc=020110%2C02

Attention product codes starting with a zero have to be entered as a string
otherwise when they are entered as a numeric variable, the first zero is truncated.

```{r}
# 2 digit product code doesn't work
meat02fr <- loadcomtradebycode("02", 76, 2018)
# 4 digit product code doesn't work either
meat0201fr <- loadcomtradebycode("0201", 76, 2018)
# 6 digit product code
meat020110fr <- loadcomtradebycode("020110", 76, 2018)
```


# Load data on the server

```{r}

```



