---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.11.3
  kernelspec:
    display_name: Python 3
    language: python
    name: python3
---

```python
import pandas as pd
import json
df = pd.read_csv('../../data/faostat/crop_production_all.csv',
                  keep_default_na = True, encoding = 'latin-1')
df = df.query('reporter == "Brazil" & element == "area_harvested" & item_code < 1000').reset_index(drop = True)
```

```python
# Using pandas group by operations
df['total_value'] = df.groupby('year')['value'].transform('sum')
df['value_percentage'] = df['value'] / df['total_value'] * 100
# Order by highest percentage first and compute a cumulative sum
df.sort_values(by=["year", "value_percentage"], ascending=[True,False], inplace=True)
df['cumsum'] = df.groupby('year').value_percentage.cumsum(skipna=True)
df['cumsum_lag'] = df.groupby('year')['cumsum'].transform('shift', fill_value=0)
```

```python
df.query("year == 2019")
```

```python
df.query("year == 2019 & cumsum_lag < 80")
```

```python

```
