---
title: "Country details"
author: "Paul Rougieux"
date:  "`r format(Sys.time(), '%d %B, %Y')`"
output:
  html_document: 
    toc: yes
---

```{r setup, include=FALSE}
# Render this notebook at the command line with
# cd ~/rp/env_impact_imports/notebooks/econometric_tests/ &&  Rscript -e "rmarkdown::render('struct_change.Rmd', 'pdf_document')"

# Render this document with less products by reducing the products in the
# vector products_of_interest

knitr::opts_chunk$set(echo = FALSE)
knitr::opts_knit$set(root.dir="../..") # file paths are relative to the root of the project directory
library(dplyr)
library(tidyr)
library(ggplot2)
```


# Introduction

Finding break dates in time series.

* [How to detect and quantify a structural break in time
  series](https://stats.stackexchange.com/questions/395078/how-to-detect-and-quantify-a-structural-break-in-time-series-r) 
* vignette from the fxregime package [Exchange Rate Regime Analysis for the Chinese Yuan](https://cran.r-project.org/web/packages/fxregime/vignettes/CNY.pdf)
* [Vignette from the structchange
  package](https://cran.r-project.org/web/packages/strucchange/vignettes/strucchange-intro.pdf).

> "Consider the standard linear regression model"

$$\label{model1} y_i = x_i^\top \beta_i + u_i \qquad (i = 1, \dots, n)$$ 

> "where at time $i$, $y_i$ is the observation
> of the dependent variable, $x_i = (1, x_{i2}, \dots, x_{ik})^\top$ is a
> $k \times 1$ vector of observations of the independent variables, with
> the first component equal to unity, $u_i$ are iid(0, $\sigma^2$), and
> $\beta_i$ is the $k \times 1$ vector of regression coefficients. Tests
> on structural change are concerned with testing the null hypothesis of
> no structural change" 

$$H_0: \quad \beta_i = \beta_0 \qquad (i = 1, \dots, n)$$ 

> "against the alternative that the coefficient vector varies over time, with
> certain tests being more or less suitable (i.e., having good or poor power)
> for certain patterns of deviation from the null hypothesis."


