---
title: "EU imports of biocommodities and products from Mercosur countries"
author: "Paul Rougieux"
date:  "`r format(Sys.time(), '%d %B, %Y')`"
output:
  word_document: 
    toc: yes
  pdf_document: 
    number_sections: yes
    toc: yes
---

```{r setup, include=FALSE}
# Render this notebook at the command line with
# cd ~/rp/env_impact_imports/notebooks/mercosur/ &&  Rscript -e "rmarkdown::render('CN8_mercosur.Rmd', 'pdf_document')"
knitr::opts_chunk$set(echo = FALSE)
knitr::opts_knit$set(root.dir="../..") # file paths are relative to the root of the project directory
library(dplyr)
library(tidyr)
library(ggplot2)
```


# Introduction

Trade at the 8 digit level for Mercosur Countries. 

# Load data


Load data from rds files (Serialization Interface for R Objects). 
See the notebook [bio_imports_update_cache.Rmd](notebooks/update_data/update_cache.Rmd) to update the cached data.

```{r}
source("R/aggregate_comext.R")
products_of_interest <- c("02", "03", "07", "09", "10", "12", "15", "17", "18", "22", "28", "40", "44", "47", "48", "52")

# Load metadata
partner_names <- readRDS("data/metadata/partner_names.rds")
reporter_names <- readRDS("data/metadata/reporter_names.rds")
product_names <- readRDS("data/metadata/product_names.rds")

# Load data from cache
system.time(
    yearly <- readRDS("data/comext/extra_eu_bio_imports.rds")
)

yearly <- yearly %>% 
    mutate(year = period %/% 100,
           productcode2d = substr(productcode, 1,2)) %>% 
    filter(productcode2d %in% products_of_interest)
```

## Filter Mercosur countries

```{r}
mercosur_country_names <- c("Argentina", "Brazil", "Paraguay", "Uruguay")
partner_names_mercosur <- partner_names  %>% 
    filter(partner %in% mercosur_country_names)
imp_mercosur <- aggregate_partners(yearly, product_names, partner_names) %>%
    filter(partnercode %in% partner_names_mercosur$partnercode) %>%
    mutate(productcode2d = substr(productcode, 1,2))
# Check we have all countries
name_differences <- setdiff(unique(imp_mercosur$partner), mercosur_country_names)
stopifnot(identical(name_differences, character(0)))
```

## Filter main products

```{r}
main_products <- aggregate_products(imp_mercosur, product_names) %>% 
    # Keep products representing together at least 60% of the total weight
    filter(lag_weight_pct_cumul < 0.6) 
```


# Time series plot

By major product groups

```{r results='asis'}
for (p2 in products_of_interest){
    product_description <- product_names$productdescription[product_names$productcode==p2]
    cat(sprintf('\n\n\\pagebreak'))
    cat(sprintf('\n\n## %s %s \n\n', p2, substr(product_description,1,57)))
    imp_mercosur_this_p2 <- imp_mercosur %>%
        filter(productcode2d == p2 & productcode %in% main_products$productcode)
    p <- imp_mercosur_this_p2 %>%
        ggplot(aes(x=year, y=tradevalue/1e6, color=productcode)) +
        geom_line()  +
        facet_wrap(~partner) +
        ylab("Trade value in million euros")
    print(p)
    p8 <- unique(imp_mercosur_this_p2$productcode)
    product_names  %>% 
        filter(product_names$productcode %in% p8) %>% 
        knitr::kable(format = 'latex') %>%
        kableExtra::column_spec(1, width = c("2cm")) %>%
        kableExtra::column_spec(2, width = c("16cm")) %>%
        print()
}
```


# Main partners at 8 digit level

List of products which represent around 80% of total trade under the given CN2 chapter (group). 

```{r results='asis'}
i = 1
for (this_product in unique(main_products$productcode)){
    i = i+1
    # if(i==5) break # Short loop for development
    # Sub section title 
    product_description <- product_names$productdescription[product_names$productcode==this_product]
    cat(sprintf('\n\n## %s %s \n\n',this_product, substr(product_description,1,57)))
    cat(product_description, '\n\n')
    # Filter only products under this CN2 code
    imp_mercosur %>% 
        filter(productcode == this_product) %>% 
        ungroup() %>% 
        arrange(year, desc(weight)) %>% 
        mutate(year = ifelse(year==lag(year, default=0),"",year)) %>% 
        mutate_at(vars(matches("pct")), ~round(.,digits = 3)*100) %>%
        select(year, partner, tradevalue, tv_pct = tradevalue_pct, weight, weight_pct) %>% 
        knitr::kable(format="pandoc", format.args = list(big.mark = ",")) %>% 
        print()
}
```

```{r eval=FALSE}

# Write to csv
write.csv(imp_mercosur, "/tmp/eu_imports_from_mercosur.csv", row.names=FALSE)

```

# Issue

## Ethanol

Ethanol products seem to be under these headings:

    22071000 Undenatured ethyl alcohol, of actual alcoholic strength of >= 80%
    22072000 Denatured ethyl alcohol and other spirits of any strength

```{r}
# Number of lines under the 2 digit level product code 22 which contains Ethanol
sum(yearly$productcode2d=="22")
sum(imp_mercosur$productcode2d=="22")
sum(main_products$productcode2d=="22")
```

