---
title: "Overview at the CN 2 chapter level"
author: "Paul Rougieux"
date: "18 March 2018"
output:
  word_document: 
    toc: yes
  pdf_document: 
    toc: yes
---

```{r setup, include=FALSE}
# Generate report from command line: 
# cd ~/rp/env_impact_imports/notebooks/CN2_overview/ &&  Rscript -e "rmarkdown::render('overview_CN2_selected.Rmd', 'pdf_document')"
library(kableExtra) # will load necessary LaTeX packages such as booktabs
library(knitr)
knitr::opts_chunk$set(echo = FALSE)
knitr::opts_knit$set(root.dir="../..") # file paths are relative to the root of the project directory
library(dplyr)
library(tidyr)
library(ggplot2)
```


# Introduction


The purpose of this document is to select the main biocommodities imported by EU countries at the CN 2 digit level. 

## Select products of interest

```{r}
products_of_interest <- c("02", "03", "07", "09", "10", "12", "15", "17", "18", "28", "40", "44", "47", "48", "52")
```


## Load data 

Load data from rds files (Serialization Interface for R Objects). 
See the notebook bio_imports_update_cache.Rmd to update the cached data.

```{r}
# Load metadata
partner_names <- readRDS("data/metadata/partner_names.rds")
reporter_names <- readRDS("data/metadata/reporter_names.rds")
product_names <- readRDS("data/metadata/product_names.rds")

product_names_HS2 <- product_names %>% 
    mutate(productcode2d = substr(productcode,1,2)) %>% 
    filter(productcode == productcode2d)

# Load data from cache
system.time(
    yearly <- readRDS("data/comext/extra_eu_bio_imports.rds")
)
# 15 seconds for 63 countries
#   user  system elapsed 
# 14.999   0.382  15.389 

yearly <- yearly %>% 
    mutate(year = period %/% 100,
           productcode2d = substr(productcode, 1,2))  %>% 
    filter(productcode2d %in% products_of_interest)
```


## Aggregate

FEATURE: calculate aggregates in the cache document, so this document generates faster?
Is this needed? 

We selected extra EU partners in the query already (see `bio_imports_update_cache.Rmd`). 
Then we aggregate trade value and weight each year at the 2 digit level. 
```{r}
yearly_agg <- yearly %>% 
    group_by(productcode2d, year) %>% 
    # Use sum(as.numeric(.)) to avoid integer overflow error
    summarise(tradevalue = sum(as.numeric(tradevalue), na.rm = TRUE),
              weight = sum(as.numeric(weight), na.rm = TRUE))

# Aggregate quantities might be relevant later they need to take care of units
# FEATURE the quantity might be pivoted to wide format along the unit and joined with the yearly_agg data frame.
yearly_agg_quantity <- yearly %>% 
    mutate(productcode2d = substr(productcode, 1,2)) %>% 
    # Add the unit code, relevant for quantity calculations
    group_by(productcode2d, unitcode, year) %>% 
    # Use sum(as.numeric(.)) to avoid integer overflow error
    summarise(quantity = sum(as.numeric(quantity),na.rm = TRUE))

# prepare table for overviez
yearly_overview <- yearly_agg %>% 
    filter(year > max(year) - 5) %>% 
    group_by(productcode2d) %>% 
    left_join(product_names_HS2, by="productcode2d") %>% 
    mutate(tradevalue_b = round(tradevalue/1e9,2),
           weight_b = round(weight/1e9,2),
           # Make product description shorter so it fits in the table
           productdescription = substr(productdescription,1,51)) %>% 
    group_by(year) %>% 
    mutate(weight_pct = round(weight/sum(weight)*100,2))
```


# Overview  last 5 years

## Trade value in billion euros
Total trade values over the last 5 years, by product chapter

```{r results='asis', warning=FALSE}
yearly_overview %>% 
    pivot_wider(id_cols = c("productcode2d", "productdescription"), names_from = year, values_from = tradevalue_b) %>% 
    knitr::kable(format = 'markdown', caption = 'Trade value in billion euros') %>%
    kableExtra::column_spec(2, width = "30em")
```


## Mass in billion kg
Total trade values over the last 5 years, by product chapter

```{r results='asis', warning=FALSE}
yearly_overview %>% 
    pivot_wider(id_cols = c("productcode2d", "productdescription"), names_from = year, values_from = weight_b) %>% 
    knitr::kable(format = 'markdown', caption = 'Trade value in billion euros') %>%
    kableExtra::column_spec(2, width = "30em")
```

### Mass in percentage

```{r}
yearly_overview %>% 
    pivot_wider(id_cols = c("productcode2d", "productdescription"), names_from = year, values_from = weight_pct) %>% 
    knitr::kable(format = 'markdown', caption = 'Trade value in billion euros') %>%
    kableExtra::column_spec(2, width = "30em")
```


# Plot time series

```{r fig.height=08, fig.width=12, results='asis'}
yearly_agg_long <- yearly_agg %>% 
    # Reshape in long format to use tradevalue and weight as facet variables
    pivot_longer(cols=c(tradevalue, weight), 
                 names_to = 'variable', values_to = 'value') %>% 
    mutate(value = value/1e9) 

# Loop
for (this_product2d in unique(yearly_agg_long$productcode2d)){
    # if(this_product2d == "03") break # shorten loop for debug
    df <-  yearly_agg_long %>% 
        filter(productcode2d == this_product2d) 
    product_description <- product_names_HS2$productdescription[product_names_HS2$productcode2d==this_product2d]
    # Make a sub-section title
    # As explained https://stackoverflow.com/a/36808845/2641825
    cat(sprintf('\n\n## %s %s \n\n',
                this_product2d, substr(product_description,1,63)))
    
    # Plot time series of trade value and weight
    p <- df %>% 
        ggplot(aes(year, value, color=productcode2d)) +
        geom_point() +
        theme(legend.position = "none") +
        facet_wrap(~variable, scales = 'free', ncol=2) +
        ylab("Tradevalues in billion EUROS and weight in billion kg") +
        ggtitle(paste(this_product2d, product_description))
    print(p)
    
    # Display summary table of value in billion euros and weight in billion kg
    yearly_agg %>% 
        filter(productcode2d == this_product2d
                & year > max(year) - 5 ) %>% 
        mutate(tradevalue_b = round(tradevalue/1e9,3),
               weight_b = round(weight/1e9,3)) %>% 
        select(-tradevalue, -weight) %>% 
        # kable(format.args = list(big.mark = ',')) %>% 
        kable(format = "markdown") %>% 
        print()
}
```


```{r eval=FALSE}
# ALl on one plot
yearly_agg_long %>% 
    ggplot(aes(year, value, color=productcode2d)) +
    geom_point() +
    theme(legend.position = "none") +
    facet_wrap(productcode2d~variable, scales = 'free', ncol=2) +
    ylab("Tradevalues in billion EUROS and weight in billion kg")
ggsave("~/downloads/HS2.pdf", width = 12, height = 20)
```



