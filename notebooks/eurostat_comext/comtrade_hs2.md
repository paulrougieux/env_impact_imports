---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.11.3
  kernelspec:
    display_name: Python 3
    language: python
    name: python3
---

# Introduction

The purpose of this notebook is to analyse Comtrade data of bioeconomy-related products at the 2 digit level.


# Load from the Comtrade API


```python
from env_impact_imports.comtrade import comtrade
# Download only and return a data frame, for debugging purposes
# Other sawnwood
wood = comtrade.pump.download(cc = "44")
wood

```

```python

```
