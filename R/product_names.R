# TODO place this in the eutradeflows package and remove source calls to this script

#' Shorten the product names
#' @description Remove the content of the parenthesis and remove content after
#' the last comma. If there are no parenthesis or commas, truncate to nchar_max
#' number of characters.
#' @param description character vector of product descriptions
#' @param nchar_max intecher maximum number of characters in the description
before_parenthesis_or_last_comma <- function(description, nchar_max = 100){

    # Remove the content of the parenthesis 
    # Parenthesis pattern (keeps the opening parenthesis in the match result)
    pattern <- '[^(]*\\)'
    m <- gregexpr(pattern, description)
    # Select the inverse of the parenthesis pattern and keep only the first match.
    description_without_parenthesis <- sapply(regmatches(description, m, invert = TRUE), `[`, 1)

    # Truncate to the maximum number of characters
    description_truncated <- substr(description_without_parenthesis, 1, nchar_max)

    # Get the position of the last comma or opening parenthesis
    commas_positions <- gregexpr(',|\\(', description_truncated)
    last_comma <- sapply(commas_positions,  max)
    # Replace -1 by the maximum number of characters
    last_comma <- ifelse(last_comma <0, nchar_max, last_comma)
    substr(description_truncated, 1, last_comma -1 )
}

