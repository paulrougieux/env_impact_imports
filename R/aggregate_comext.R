# TODO: move these functions to the eutradeflows package once they are in a stable state. 
# TODO: take into account changes in product codes. 

#' Function to extract main products from a tradeflows table
#' @description Calculate the percentage of trade within each CN2 product group and each year.
#' Compute cumulative weight percentage.
#' @param df data frame of yearly trade flows as available in "data/comext/extra_eu_bio_imports.rds"
#' @param product_names data frame containing product names
#' @return data frame of main products
aggregate_products <- function(df, product_names){
    df %>% 
        filter(year > max(year) - 5) %>% 
        # Calculate the sum for each CN8 product code 
        group_by(year, productcode2d, productcode) %>% 
        summarise(tradevalue = sum(tradevalue, na.rm = TRUE),
                  weight = sum(weight, na.rm = TRUE),
                  tradevalue_b = round(sum(tradevalue)/1e9,1),
                  .groups = "drop") %>% 
        # Calculate the percentage of trade within each CN2 product group and each year
        group_by(productcode2d, year) %>% 
        # compute cumulative trade percentage
        arrange(productcode2d, year, desc(tradevalue))  %>% 
        mutate(sumtradevalue = sum(tradevalue, na.rm = TRUE),
               tradevalue_pct = round(tradevalue / sumtradevalue,2),
               tradevalue_pct_cumul = cumsum(tradevalue_pct)) %>% 
        # Compute cumulative weight percentage
        arrange(productcode2d, year, desc(weight))  %>% 
        mutate(sumweight = sum(weight, na.rm = TRUE),
               weight_pct = round(weight / sumweight,2),
               lag_weight_pct_cumul = lag(cumsum(weight_pct), default=0)) %>% 
        left_join(product_names, by="productcode") %>%
        mutate(productdescription = substr(productdescription, 1, 20)) 
}

#' Function to aggregate partners from a tradeflows table
#' It aggregates all reporter countries together.
#' @param df data frame of yearly trade flows as available in "data/comext/extra_eu_bio_imports.rds"
#' @rdname extract_main_products
aggregate_partners <- function(df, product_names, partner_names){
    df %>% 
        # Calculate the sum for each CN8 product code 
        group_by(year, productcode, partnercode, partneriso) %>% 
        summarise(weight = sum(weight, na.rm = TRUE),
                  tradevalue = sum(tradevalue),
                  weight_b = round(sum(weight)/1e9,1),
                  .groups = "drop") %>% 
        # Calculate the percentage of trade between partners within each CN8 product group and each year
        group_by(productcode, year) %>% 
        arrange(productcode, year, desc(weight))  %>% 
        mutate(sumweight = sum(weight, na.rm = TRUE),
               weight_pct = weight / sumweight,
               weight_pct_cumul = cumsum(weight_pct),
               sumtradevalue = sum(tradevalue, na.rm = TRUE),
               tradevalue_pct = tradevalue / sumtradevalue,
               tradevalue_pct_cumul = cumsum(tradevalue_pct)) %>% 
        left_join(product_names, by="productcode") %>%
        left_join(partner_names, by="partnercode") %>% 
        mutate(productdescription = substr(productdescription, 1, 20)) 
}
