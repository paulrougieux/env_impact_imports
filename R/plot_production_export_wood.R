library(dplyr)
library(reshape2)
library(ggplot2)
library(tidyr)
library("gridExtra") 

##########################################
###### Forestry production
##########################################

df_forestry <- read.csv("data/faostat/forestry_production_selected.csv", stringsAsFactors=FALSE)

df_forestry <- filter(df_forestry, year>=2000)
# table(df_forestry$product)

df_forestry <- df_forestry %>% 
    filter(element == 'production' | element == "export_quantity") %>%
    filter(grepl('m3', unit))

trop_name <- 'Industrial roundwood, non-coniferous tropical (export/import)'
rwd <- filter(df_forestry, 
              product %in% c('Roundwood', trop_name))  %>%
       mutate(product = replace(product, product==trop_name, 'trop'))

swd <- filter(df_forestry, product=='Sawnwood')


# Select production export and tropical timber export



##########################################
###### Forestry Trade and Flows
##########################################

df_trade <- read.csv("data/faostat/forestry_trade_flows_selected.csv", stringsAsFactors=FALSE)
df_trade <- filter(df_trade, year>=2000)


countries_with_large_degradation <- 
  c("Brazil", "Indonesia", "Democratic Republic of the Congo", "Colombia",
    "Myanmar", "Bolivia (Plurinational State of)", "Malaysia", "Peru",
    "Philippines", "Papua New Guinea", "Lao People's Democratic Republic",
    "Viet Nam", "Venezuela (Bolivarian Republic of)", "Mexico", "Angola",
    "India", "Cote d'Ivoire", "Thailand", "Madagascar", "Ecuador", "Ghana",
    "Liberia", "Nigeria", "Nicaragua", "Central African Republic",
    "Cameroon")
setdiff(countries_with_large_degradation, unique(df_trade$reporter))

# TODO correct Cote d'Ivoire in trade data input

eu_countries <- c("Austria", "Belgium", "Bulgaria", "Croatia", "Cyprus",
                  "Czechia", "Denmark", "Estonia", "Finland", "France",
                  "Germany", "Greece", "Hungary", "Ireland", "Italy", "Latvia",
                  "Lithuania", "Luxembourg", "Malta", "Netherlands", "Poland",
                  "Portugal", "Romania", "Slovakia", "Slovenia", "Spain",
                  "Sweden")


df_trade <- subset(df_trade, reporter %in% countries_with_large_degradation)

#Trade to rest of the world or Trade to EU
df_trade <- subset(df_trade, partner %in% eu_countries)
df_trade <- df_trade %>% filter(grepl('export_quantity', element)) 

df_trade_EU <- df_trade %>%
  group_by(reporter,product,year) %>%
  summarize(Exportquantity = sum(value))

df_trade_swd <- df_trade_EU %>% 
  filter(grepl('Sawnwood', product))%>%
  group_by(reporter,year) %>%
  summarize(Exportquantity_Swn = sum(Exportquantity))

df_trade_rwd <- df_trade_EU %>% 
  filter(grepl('roundwood', product))


df_trade_rwd_tot <- df_trade_rwd %>% 
  group_by(reporter,year) %>%
  summarize(Exportquantity_Rwd = sum(Exportquantity))



# ##########################################
# ###### Merge production and export data
# ##########################################
# 
# 
# df_roundwood <- merge(rwd, df_trade_simp, by.x=c("area", "year"), by.y=c("reporter", "year"))
# df_roundwood <- df_roundwood[ , c("area","year","value","Exportquantity")]
# colnames(df_roundwood)[3:4]=c('production','Export')
# 
# df_roundwood_long <- melt(data=df_roundwood, id.vars=c("area","year"),variable.name="Type",value.name='Value')

##########################################
###### Plots
##########################################

#Plot both production and export to rest of the world
selected_countries = c("Brazil")

########### ROUNDWOOD #######################################################


# Plot trade dataset Export of tropical non-coniferous Industrial roundwood to EU countries
gp1= df_trade_rwd %>%
  filter(reporter %in% selected_countries) %>%
  filter(grepl(' non-coniferous tropical ', product))%>%
  ggplot(aes(x = year, y = Exportquantity/1e6, fill=product)) +
  geom_bar(stat='identity') +
  scale_fill_manual(values = c("grey")) +
  #facet_wrap(~product_element, ncol=1, scales="free_y",labeller=labeller(product_element = c("Roundwood_export_quantity" = "Roundwood export quantity", "Roundwood_production" = "Roundwood production","trop_export_quantity" = "Tropical roundwood export quantity"))) +
  ggtitle(sprintf("Export of tropical roundwood to EU countries")) +
  theme_minimal() +
  theme(legend.position = "none")+
  #theme(legend.position="bottom") +
  labs(y = "Quantity in million m3",
       fill = "Roundwood product group")


# Plot trade dataset Export of Roundwood to EU countries
gp2= df_trade_rwd_tot %>%
  filter(reporter %in% selected_countries) %>%
  ggplot(aes(x = year, y = Exportquantity_Rwd/1e6)) +
  geom_bar(stat='identity') +
  scale_fill_manual(values = c("grey")) +
  #facet_wrap(~product_element, ncol=1, scales="free_y",labeller=labeller(product_element = c("Roundwood_export_quantity" = "Roundwood export quantity", "Roundwood_production" = "Roundwood production","trop_export_quantity" = "Tropical roundwood export quantity"))) +
  ggtitle(sprintf("Export of roundwood to EU countries")) +
  theme_minimal() +
  theme(legend.position = "none")+
  #theme(legend.position="bottom") +
  labs(y = "Quantity in million m3",
       fill = "Roundwood product group")



# Plot roundwood production and export, including tropical rwd
rwd_selected <- rwd %>%
  filter(reporter %in% selected_countries) %>%
  unite(product_element, product, element)

neworder <- c("Roundwood_production","Roundwood_export_quantity","trop_export_quantity")

rwd_selected <- arrange(mutate(rwd_selected,
                               product_element=factor(product_element,levels=neworder)),product_element)

ggp1 = rwd_selected %>%
  ggplot(aes(x = year, y = value/1e6, fill=product_element)) +
  geom_bar(stat='identity') +
  scale_fill_manual(values = c("brown", "orange", "salmon")) +
  facet_wrap(~product_element, ncol=1, scales="free_y",labeller=labeller(product_element = c("Roundwood_export_quantity" = "Roundwood export quantity", "Roundwood_production" = "Roundwood production","trop_export_quantity" = "Tropical roundwood export quantity"))) +
  ggtitle(sprintf("production and export of roundwood from %s", selected_countries)) +
  theme_minimal() +
  theme(legend.position = "none")+
  #theme(legend.position="bottom") +
  labs(y = "Quantity in million m3",
       fill = "Roundwood product group")


x= grid.arrange(gp1,gp2, ncol = 1)    
grid.arrange(ggp1,x, ncol = 2)  



########### SAWNWOOD #######################################################


# Plot sawnwood production and export
swd_selected <- swd %>%
  filter(reporter %in% selected_countries) 

neworder2 <- c("production","export_quantity")

swd_selected <- arrange(mutate(swd_selected,
                               element=factor(element,levels=neworder2)),element)

ggp2 = swd_selected %>%
  ggplot(aes(x = year, y = value/1e6, fill=element)) +
  geom_bar(stat='identity') +
  scale_fill_manual(values = c("brown", "orange")) +
  facet_wrap(~element, ncol=1, scales="free_y",,labeller=labeller(element = c("export_quantity" = "Sawnwood export quantity", "production" = "Sawnwood production")))  +
  ylab("Quantity in million m3") +
  ggtitle(sprintf("production and export of sawnwood from %s", selected_countries)) +
  theme_minimal() +
  theme(legend.position="none") +
  labs(y = "Quantity in million m3",
       fill = "Roundwood product group")
theme(legend.title = element_blank())


# Plot trade dataset Export of sawnwood to EU countries
ex= df_trade_swd %>%
  filter(reporter %in% selected_countries) %>%
  ggplot(aes(x = year, y = Exportquantity_Swn/1e6)) +
  geom_bar(stat='identity') +
  scale_fill_manual(values = c("salmon")) +
  #facet_wrap(~product_element, ncol=1, scales="free_y",labeller=labeller(product_element = c("Roundwood_export_quantity" = "Roundwood export quantity", "Roundwood_production" = "Roundwood production","trop_export_quantity" = "Tropical roundwood export quantity"))) +
  ggtitle(sprintf("Export of sawnwood to EU countries")) +
  theme_minimal() +
  theme(legend.position = "none")+
  #theme(legend.position="bottom") +
  labs(y = "Quantity in million m3",
       fill = "Roundwood product group")

grid.arrange(ggp2,ex, ncol = 2)    






# 
# # Plot based on join dataset
# df_roundwood_long %>%
#   filter(reporter %in% selected_countries) %>%
#   ggplot(aes(x = year, y = Value, fill=Type)) +
#   geom_bar(stat='identity') +
#   scale_fill_manual(values = c("brown", "green")) +
#   theme_minimal() +
#   theme(legend.position="bottom") +
#   labs(y = "Quantity (m3)",
#        fill = "Roundwood product group")
# 
# #Plot only export to rest of the world
# selected_countries = c("Brazil")
# selected_type = c("Export")
# 
# df_roundwood_long %>%
#   filter(reporter %in% selected_countries) %>%
#   filter(Type %in% selected_type) %>%
#   ggplot(aes(x = year, y = Value, fill=Type)) +
#   geom_bar(stat='identity') +
#   scale_fill_manual(values = c("brown", "green")) +
#   theme_minimal() +
#   theme(legend.position="bottom") +
#   labs(y = "Quantity (m3)",
#        fill = "Roundwood product group")
