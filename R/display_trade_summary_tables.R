source("R/product_names.R")

#' Extract a trade summary table for the main trade partner and the main products under each commodity
#' @description Given a selection of commodities, select the main partner countries. 
#' The list of products at the 8 digit level was selected by choosing products
#' which represent around 80% of total trade under the given CN2 chapter (group). 
#' @param product_threshold numeric threshold value [0,1] to select CN8
#' products representing together at least this percentage of total weight 
#' @param partner_threshold numeric threshold value [0,1] to select import
#' partners representing together at leas this percentage of total trade value
#' (because the weight is sometimes not available for some partners) 
#' @example
#' \dontrun{
#' trade_summary_CN8(imp, product_threshold = 0.6, partner_threshold = 0.8, product_names, partner_names)
#' }
trade_summary_CN8 <- function(df, 
                                    product_threshold, 
                                    partner_threshold, 
                                    product_names, 
                                    partner_names, 
                                    short_name_title = TRUE){

    # Keep only the main CN8 products under each CN2 product group
    main_products <- aggregate_products(df, product_names) %>% 
        # Keep products representing together at least 60% of the total weight
        filter(lag_weight_pct_cumul < product_threshold) 

    # Aggregate flows for reporter countries together
    imp_by_partners <- aggregate_partners(df, product_names, partner_names)

    # Create a list of the main partners for each CN8 product
    main_partners <- imp_by_partners %>% 
        # Keep only main products
        filter(productcode %in% main_products$productcode) %>% 
        # Keep partners representing together at least 80% of the total trade value 
        # Because the weight is sometimes missing for some partners
        filter(lag(tradevalue_pct_cumul, default=0) < partner_threshold) %>% 
        group_by(productcode, partnercode, partneriso) %>% 
        tally()

    # Keep only the main partners for each CN8 product
    imp_by_main_partners <- imp_by_partners %>% 
        # Filter using a righ join
        right_join(main_partners, by=c("productcode", "partnercode", "partneriso"))

    return(imp_by_main_partners)
}


#' @rdname trade_summary_CN8
#' @description Display a markdown tables for the main trade partner and the
#' main CN8 products under each CN2 product group  
#' @param imp_by_main_partners data frame output of the function display_trade_summary_CN8
#' @param products_of_interest short table as defined in R/cache_selected_products_and_years.R 
#' @param short_name_title boolean whether or not to display a level one title with short names (choose FALSE when there is only one CN2 product in the page)
#' @example
#' \dontrun{
#' display_trade_summary_table_CN8(imp, product_threshold = 0.6, partner_threshold = 0.8, product_names, partner_names)
#' }
display_trade_summary_table_CN8 <- function(imp_by_main_partners,
                                            products_of_interest,
                                            product_names, 
                                            short_name_title = TRUE){
    last_short_name <- ""
    all_product_codes <- unique(imp_by_main_partners$productcode)

    for (this_product in all_product_codes){
        # Short loop for development
        # if(this_product == all_product_codes[3]) break

        # Use the short name as a top level section title in the web page
        if (short_name_title){
            this_short_name <- products_of_interest$shortname[products_of_interest$productcode2d == substr(this_product, 1, 2)]
            # Do not repeat the section title for several consecutive products sharing a 2d code.
            # Only show it when it changes.
            if (this_short_name != last_short_name){
                cat(sprintf('\n\n# %s \n\n', this_short_name))
                last_short_name <- this_short_name
            }
        }

        # Use the beginning of the CN8 product name as sub section title 
        product_description <- product_names$productdescription[product_names$productcode==this_product]
        product_description_short <- before_parenthesis_or_last_comma(product_description, 100)
        cat(sprintf('\n\n## %s %s (table)\n\n', this_product, product_description_short))
        cat(product_description, '\n\n')
        # Filter only products under this CN8 code
        imp_by_main_partners %>% 
            filter(productcode == this_product) %>% 
            ungroup() %>% 
            arrange(year, desc(weight)) %>% 
            # Replace repeated years by empty fields
            mutate(year = ifelse(year==lag(year, default=0),"",year),
                   partner = substr(partner, 1, 25)) %>% 
            mutate_at(vars(matches("pct")), ~round(.,digits = 3)*100) %>%
            select(year, partner, tradevalue, tv_pct = tradevalue_pct, weight, weight_pct) %>% 
            knitr::kable(format="pandoc", format.args = list(big.mark = ",")) %>% 
            print()
    }
}


